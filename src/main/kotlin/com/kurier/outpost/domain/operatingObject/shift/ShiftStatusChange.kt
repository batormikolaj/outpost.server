package com.kurier.outpost.domain.operatingObject.shift

import com.kurier.outpost.domain.database.activity.ShiftStatus

class ShiftStatusChange {
    var idShift: Long = 0
    val status: ShiftStatus? = null
}