package com.kurier.outpost.domain.operatingObject.employee

import com.kurier.outpost.domain.database.general.User
import com.kurier.outpost.domain.database.general.Address
import com.kurier.outpost.domain.database.general.LoginObject
import com.kurier.outpost.domain.database.general.Person

class EmployeeWrapper(val address: Address,
                      val person: Person,
                      val user: User,
                      var loginObject: LoginObject)