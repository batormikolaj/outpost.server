package com.kurier.outpost.domain.operatingObject.employee

import com.kurier.outpost.domain.database.employee.Administrator
import com.kurier.outpost.domain.database.employee.Carrier
import com.kurier.outpost.domain.database.employee.Dispatcher

class AllEmployeesObject(val carriers:List<Carrier>,
                         val dispatchers:List<Dispatcher>,
                         val administrators:List<Administrator>) {
    companion object {
        class AllEmployeesObjectBuilder{
            lateinit var carriers:List<Carrier>
            lateinit var dispatchers:List<Dispatcher>
            lateinit var administrators:List<Administrator>

            fun setCarriers(carriers: List<Carrier>): AllEmployeesObjectBuilder {
                this.carriers=carriers
                return this
            }

            fun setDispatchers(dispatchers: List<Dispatcher>): AllEmployeesObjectBuilder {
                this.dispatchers=dispatchers
                return this
            }

            fun setAdministrators(administrators: List<Administrator>): AllEmployeesObjectBuilder {
                this.administrators=administrators
                return this
            }

            fun build() = AllEmployeesObject(carriers, dispatchers, administrators)
        }
    }
}