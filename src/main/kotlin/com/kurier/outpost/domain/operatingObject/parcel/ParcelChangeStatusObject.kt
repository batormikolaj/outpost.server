package com.kurier.outpost.domain.operatingObject.parcel

import com.kurier.outpost.domain.database.parcel.ParcelStatus

class ParcelChangeStatusObject {
    var id: Long = 0
    val status: ParcelStatus? = null
}