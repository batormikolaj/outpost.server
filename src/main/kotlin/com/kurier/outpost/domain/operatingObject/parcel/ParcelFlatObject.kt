package com.kurier.outpost.domain.operatingObject.parcel

import com.kurier.outpost.domain.database.parcel.ParcelGauge

class ParcelFlatObject {
    var senderId: Long? = null
    var recipientId: Long? = null

    var recipientName: String = ""
    var recipientSurname: String = ""
    var recipientTel: Int = 0
    var recipientEmail: String = ""

    var recipientStreet: String = ""
    var recipientStreetNumber: Int = 0
    var recipientHouseNumber: Int = 0
    var recipientCity: String = ""
    var recipientPostalCode: String = ""

    var gauge: ParcelGauge? = null
    var weight: Double = 0.0

}