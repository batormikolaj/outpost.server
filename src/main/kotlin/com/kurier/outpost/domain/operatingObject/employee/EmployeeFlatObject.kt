package com.kurier.outpost.domain.operatingObject.employee

open class EmployeeFlatObject {
    var type: String = ""

    var name: String = ""
    var surname: String = ""
    var telNumber: Int = 0
    var email: String = ""

    var street: String = ""
    var street_number: Int = 0
    var house_number: Int = 0
    var city: String = ""
    var postal_code: String = ""

}