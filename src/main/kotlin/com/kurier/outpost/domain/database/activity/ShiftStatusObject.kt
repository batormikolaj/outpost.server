package com.kurier.outpost.domain.database.activity

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
class ShiftStatusObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0
    var timestamp: Long = 0
    var status: ShiftStatus? = null

    @ManyToOne(cascade = [CascadeType.ALL])
    @JsonIgnore
    var shift: Shift? = null
}