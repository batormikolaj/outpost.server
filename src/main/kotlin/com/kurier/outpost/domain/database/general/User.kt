package com.kurier.outpost.domain.database.general

import com.fasterxml.jackson.annotation.JsonIgnore
import com.kurier.outpost.domain.database.general.LoginObject
import com.kurier.outpost.domain.database.general.Person
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0
    @OneToOne(cascade = [(CascadeType.ALL)])
    var personalia: Person? = null

    @OneToOne
    @JsonIgnore
    var loginObject: LoginObject? = null

    override fun toString(): String {
        return "id: $id, personalia.telNr=${personalia!!.telNumber}"
    }
}