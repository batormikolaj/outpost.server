package com.kurier.outpost.domain.database.activity

import com.kurier.outpost.domain.database.employee.Carrier
import com.kurier.outpost.domain.database.parcel.Parcel
import java.time.Instant
import javax.persistence.*

@Entity
class Shift {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0
    var name: String = ""

    @OneToMany
    var shiftStatus: MutableCollection<ShiftStatusObject>? = null

    @OneToMany
    var parcels: MutableCollection<Parcel>? = null

//    @OneToMany //(cascade = [(CascadeType.ALL)])
//    @ManyToOne
//    var carriers: Carrier? = null
}