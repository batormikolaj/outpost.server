package com.kurier.outpost.domain.database.parcel

enum class ParcelStatus {
    PREPARED, PICKED_UP, ON_WAY_TO_MAGAZINE, IN_MAGAZINE, ON_WAY_TO_RECEIVER, DELIVERED
}