package com.kurier.outpost.domain.database.employee

import com.fasterxml.jackson.annotation.JsonIgnore
import com.kurier.outpost.domain.database.activity.Shift
import com.kurier.outpost.domain.database.general.User
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name="CARRIER")
class Carrier : User() {
//    @JsonIgnore
@OneToMany //(cascade = [(CascadeType.ALL)])
var shifts: MutableCollection<Shift>? = null
}