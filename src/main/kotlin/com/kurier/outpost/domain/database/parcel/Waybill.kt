package com.kurier.outpost.domain.database.parcel

import com.kurier.outpost.domain.database.general.Address
import com.kurier.outpost.domain.database.general.Person
import javax.persistence.*

@Entity
class Waybill {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
    @OneToOne
    var sender: Person? = null
    @OneToOne
    var recipient: Person? = null
}