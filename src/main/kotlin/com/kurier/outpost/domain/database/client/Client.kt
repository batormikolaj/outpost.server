package com.kurier.outpost.domain.database.client

import com.kurier.outpost.domain.database.general.User
import com.kurier.outpost.domain.database.parcel.Parcel
import javax.persistence.*

@Entity
class Client : User() {

//    @OneToOne
//    val address:Address?=null
    @OneToMany
    val history:MutableCollection<Parcel>?=null

    @OneToMany
    val predefined: MutableCollection<Client>? = null
}