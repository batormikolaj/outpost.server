package com.kurier.outpost.domain.database.activity

enum class ShiftStatus {
    CREATED, IN_PROGRESS, DONE
}