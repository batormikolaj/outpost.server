package com.kurier.outpost.domain.database.general

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    val id:Long=0
    var street:String=""
    var street_number:Int=0
    var house_number: Int? = 0
    var city:String=""
    var postal_code:String=""

    override fun toString(): String {
        return "Address(id=$id, street='$street', street_number=$street_number, house_number=$house_number, city='$city', postal_code='$postal_code')"
    }

}