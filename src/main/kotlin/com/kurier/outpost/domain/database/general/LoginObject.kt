package com.kurier.outpost.domain.database.general

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToOne

@Entity
class LoginObject {
    @Id
    var login: String = ""
    var passHash: String = ""

    @OneToOne
    var owner: User? = null
}