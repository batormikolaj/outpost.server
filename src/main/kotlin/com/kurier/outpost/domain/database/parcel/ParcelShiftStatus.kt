package com.kurier.outpost.domain.database.parcel

enum class ParcelShiftStatus {
    NOT_CONNECTED, CONNECTED
}