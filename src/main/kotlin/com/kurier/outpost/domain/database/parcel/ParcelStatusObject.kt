package com.kurier.outpost.domain.database.parcel

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
class ParcelStatusObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long = 0

    var timestamp: Long = 0
    var status: ParcelStatus? = null

    @ManyToOne
    @JsonIgnore
    var parcel: Parcel? = null
}