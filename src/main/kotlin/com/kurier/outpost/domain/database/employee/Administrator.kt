package com.kurier.outpost.domain.database.employee

import com.kurier.outpost.domain.database.general.User
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "ADMINISTRATOR")
class Administrator : User() {
}