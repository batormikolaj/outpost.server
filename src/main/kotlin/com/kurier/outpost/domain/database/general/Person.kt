package com.kurier.outpost.domain.database.general

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    val id:Long=0

    var name:String=""
    var surname:String=""
    var telNumber:Int=0
    var email: String = ""

    @OneToOne(cascade = [(CascadeType.ALL)])
    var address:Address?=null

    override fun toString(): String {
        return "Person(name='$name', surname='$surname', telNumber=$telNumber, email=$email)"
    }


}