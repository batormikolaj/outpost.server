package com.kurier.outpost.domain.database.parcel

import com.kurier.outpost.domain.database.employee.Carrier
import javax.persistence.*

@Entity
class Parcel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0
    @OneToOne
    var carrier: Carrier? = null
    @OneToMany
    var status: MutableCollection<ParcelStatusObject>? = null
    @OneToOne
    var waybill: Waybill? = null
    var gauge: ParcelGauge? = null
    var weight: Double = 0.0
    var parcelShiftStatus: ParcelShiftStatus = ParcelShiftStatus.NOT_CONNECTED
}