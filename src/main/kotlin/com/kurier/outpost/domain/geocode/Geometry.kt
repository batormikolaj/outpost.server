package com.kurier.outpost.domain.geocode

import com.fasterxml.jackson.annotation.JsonIgnore
import com.kurier.outpost.domain.geocode.Location

class Geometry {
    private var location: Location? = null
    private var location_type: String? = null
    @JsonIgnore
    private var bounds: Any? = null
    @JsonIgnore
    private var viewport: Any? = null

    fun getLocation(): Location? {
        return location
    }

    fun setLocation(location: Location) {
        this.location = location
    }

    fun getLocation_type(): String? {
        return location_type
    }

    fun setLocation_type(locationType: String) {
        this.location_type = locationType
    }

    fun getBound(): Any? {
        return bounds
    }

    fun setBounds(bounds: Any) {
        this.bounds = bounds
    }

    fun getViewport(): Any? {
        return viewport
    }

    fun setViewport(viewport: Any) {
        this.viewport = viewport
    }

}