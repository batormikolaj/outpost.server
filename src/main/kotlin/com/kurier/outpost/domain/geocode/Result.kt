package com.kurier.outpost.domain.geocode

import com.fasterxml.jackson.annotation.JsonIgnore

class Result {
    private var formatted_address: String? = null
    private var partialMatch: Boolean = false
    private var geometry: Geometry? = null
    @JsonIgnore
    private var address_components: Any? = null
    @JsonIgnore
    private var types: Any? = null

    fun getFormatted_address(): String? {
        return formatted_address
    }

    fun setFormatted_address(formattedAddress: String) {
        this.formatted_address = formattedAddress
    }

    fun isPartioalMatch(): Boolean {
        return partialMatch
    }

    fun setPartialMatch(partialMatch: Boolean) {
        this.partialMatch = partialMatch
    }

    fun getGeometry(): Geometry? {
        return geometry
    }

    fun setGeometry(geometry: Geometry) {
        this.geometry = geometry
    }

    fun getAddress_components(): Any? {
        return address_components
    }

    fun setAddres_components(addressComponents: Any) {
        this.address_components = addressComponents
    }

    fun getTypes(): Any? {
        return types
    }

    fun setTypes(types: Any) {
        this.types = types
    }


}
