package com.kurier.outpost.domain.geocode

class Location {
    private var latitude: String? = null
    private var longitude: String? = null

    fun getLat(): String? {
        return latitude
    }

    fun setLat(latitude: String) {
        this.latitude = latitude
    }

    fun getLng(): String? {
        return longitude
    }

    fun setLng(longitude: String) {
        this.longitude = longitude
    }

}