package com.kurier.outpost.domain.geocode

import com.kurier.outpost.domain.geocode.Result

class GoogleResponse {
    private var results: Array<Result>? = null
    private var status: String? = null

    fun getResults(): Array<Result>? {
        return this.results
    }

    fun setResults(results: Array<Result>) {
        this.results = results
    }

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }


}