package com.kurier.outpost

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class OutpostApplication
val log = LoggerFactory.getLogger(OutpostApplication::class.java)
fun main(args: Array<String>) {
    runApplication<OutpostApplication>(*args)
}
