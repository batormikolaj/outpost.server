package com.kurier.outpost.service

import com.kurier.outpost.component.AddressToGeocodeConverter
import com.kurier.outpost.component.DatabaseProvider
import com.kurier.outpost.component.FlatToObjectFactory
import com.kurier.outpost.domain.database.general.Address
import com.kurier.outpost.domain.database.general.Person
import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.database.parcel.ParcelStatus
import com.kurier.outpost.domain.database.parcel.ParcelStatusObject
import com.kurier.outpost.domain.database.parcel.Waybill
import com.kurier.outpost.domain.operatingObject.parcel.ParcelAddressFlatObject
import com.kurier.outpost.domain.operatingObject.parcel.ParcelFlatObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class ParcelService {

    @Autowired
    lateinit var databaseProvider: DatabaseProvider

    @Autowired
    lateinit var flatToObjectFactory: FlatToObjectFactory

    @Autowired
    lateinit var addressConverter: AddressToGeocodeConverter

    fun changeStatusOfParcel(id: Long, status: ParcelStatus) {
        databaseProvider.changeStatusOfParcel(id, status)
    }

    fun addParcel(parcelFlatObject: ParcelFlatObject) {
        val parcel = Parcel()
        val waybill = Waybill()
        val nadawca = databaseProvider.getClientById(parcelFlatObject.senderId!!).personalia!!
//        val address_nadawca = flatToObjectFactory.addressFactory(city = "Krakow", house_number = 55, street_number = 44, street = "Focha", postal_code = "33-213")
//        val person_nadawca = flatToObjectFactory.personFactory(name = "Jan", surname = "Kowalski", telNumber = 444123123, email = "JanKowalski@outpost.com")
//        person_nadawca.address = address_nadawca
        waybill.sender = nadawca
        val nadawcaObject = databaseProvider.getClientById(parcelFlatObject.senderId!!)
        val addressRecipient: Address
        val recipient: Person
        if (parcelFlatObject.recipientId == null) {
            addressRecipient = flatToObjectFactory.addressFactory(street = parcelFlatObject.recipientStreet,
                    street_number = parcelFlatObject.recipientStreetNumber,
                    house_number = parcelFlatObject.recipientHouseNumber,
                    city = parcelFlatObject.recipientCity,
                    postal_code = parcelFlatObject.recipientPostalCode)

            recipient = flatToObjectFactory.personFactory(name = parcelFlatObject.recipientName,
                    surname = parcelFlatObject.recipientSurname,
                    telNumber = parcelFlatObject.recipientTel,
                    email = parcelFlatObject.recipientEmail)
            recipient.address = addressRecipient
            waybill.recipient = recipient
        } else {
            recipient = databaseProvider.getClientById(parcelFlatObject.recipientId!!).personalia!!
            addressRecipient = recipient.address!!
            waybill.recipient = recipient
        }


        //TODO:how to create it as default
        val statusObject = ParcelStatusObject()
        statusObject.timestamp = Instant.now().epochSecond
        statusObject.status = ParcelStatus.PREPARED
        val list = mutableListOf<ParcelStatusObject>()
        list.add(statusObject)
        parcel.waybill = waybill
        parcel.status = list
        parcel.gauge = parcelFlatObject.gauge
        parcel.weight = parcelFlatObject.weight

//        databaseProvider.saveAddress(address_nadawca)
        databaseProvider.saveAddress(addressRecipient)
//        databaseProvider.savePerson(person_nadawca)
        databaseProvider.savePerson(recipient)
        databaseProvider.saveWaybill(waybill)
        databaseProvider.saveParcelStatus(statusObject)
        databaseProvider.saveParcel(parcel)
        databaseProvider.saveParcelIdToStatus(parcel.id, statusObject.id)
        nadawcaObject.history!!.add(parcel)
        databaseProvider.saveClient(nadawcaObject)
    }

    fun getAllParcels(): List<Parcel> {
        return databaseProvider.getAllParcels()
    }

    fun getParcelStatus(id: Long): ParcelStatusObject {
        return databaseProvider.getParcelStatus(id)

    }
//  TODO XD Optional<List
    fun getParcelHistory(parcelId: Long): MutableCollection<ParcelStatusObject> {
        return databaseProvider.getAllStatusParcel(parcelId)
    }

    fun getParcel(id: Long): Parcel {
        return databaseProvider.getParcel(id)
    }

    fun getParcelsByStatus(parcelStatus: ParcelStatus) :MutableCollection<Parcel> {
        val listIdParcels = databaseProvider.getParcelsByStatus(parcelStatus)
        val listParcels = arrayListOf<Parcel>()

        listIdParcels.forEach {
            listParcels.add(databaseProvider.getParcel(it))
        }

        return listParcels
    }

    fun findByShiftId(idShift: Long): MutableCollection<Parcel>? {
        return databaseProvider.findParcelByShiftI(idShift)
    }

    fun getGeocode(address: ParcelAddressFlatObject): String {
        val res = addressConverter.convertToGeocode(
                address.street + "," +
                        address.streetNumber + "," +
                        address.city + "," +
                        address.voivodeship + "," +
                        address.country)

        var response = ""
        if (res.getStatus() == "OK") {
            for (result in res.getResults()!!) {
                response = result.getGeometry()!!.getLocation()!!.getLat()!! + "," +
                        result.getGeometry()!!.getLocation()!!.getLng()!!
            }
        } else {
            response = "ERROR"
        }

        return response
    }
}