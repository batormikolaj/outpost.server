package com.kurier.outpost.service

import com.kurier.outpost.component.*
import com.kurier.outpost.domain.database.client.Client
import com.kurier.outpost.domain.database.employee.Administrator
import com.kurier.outpost.domain.database.employee.Carrier
import com.kurier.outpost.domain.database.employee.Dispatcher
import com.kurier.outpost.domain.database.general.LoginObject
import com.kurier.outpost.domain.operatingObject.user.UserFlatObject
import com.kurier.outpost.repository.UserRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class LoginService {
    @Autowired
    lateinit var defaultLoginGenerator: DefaultLoginGenerator

    @Autowired
    lateinit var loginService: LoginService

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var flatToObjectFactory: FlatToObjectFactory

    @Autowired
    lateinit var hashGenerator: HashGenerator

    @Autowired
    lateinit var databaseProvider: DatabaseProvider

    @Autowired
    lateinit var restJTWTProvider: RestJTWTProvider

    fun addUser(userFlat: UserFlatObject) {
        val client = Client()
        val address = flatToObjectFactory.addressFactory(street = userFlat.street,
                street_number = userFlat.street_number,
                house_number = userFlat.house_number,
                city = userFlat.city,
                postal_code = userFlat.postal_code)
        val person = flatToObjectFactory.personFactory(name = userFlat.name,
                surname = userFlat.surname,
                telNumber = userFlat.telNumber,
                email = userFlat.email)
        person.address = address
        client.personalia = person

        val loginObject = defaultLoginGenerator.generateDefaultLoginObject(userFlat.name, userFlat.surname)
        client.loginObject = loginObject

        databaseProvider.saveAddress(address)
        databaseProvider.savePerson(person)
        databaseProvider.saveLoginObject(loginObject)
        databaseProvider.saveClient(client)
        loginObject.owner = client
        databaseProvider.saveLoginObject(loginObject)
    }

    fun login(login: String?, pass: String?): String? {

        val loginObject = LoginObject()
        loginObject.login = login!!
        loginObject.passHash = hashGenerator.createHash(pass!!)
        //TODO: Provide "connection" between loginObject and User

        if (!databaseProvider.exists(login)) {
            throw RuntimeException("User does not exist")
        }
        val loginObjectInDB = databaseProvider.getLogin(login)
        if (!loginObject.passHash.contains(loginObjectInDB.passHash)) {
            throw RuntimeException("Password incorrect")
        }

        val user = loginObjectInDB.owner
        val role = when (user) {
            is Client -> "ROLE_USER"
            is Carrier -> "ROLE_CARRIER"
            is Dispatcher -> "ROLE_DISPATCHER"
            is Administrator -> "ROLE_ADMIN"
            else -> throw RuntimeException("NIe ma")
        }

        log.info("hash from user's pass: ${loginObject.passHash}, pass from DB: ${loginObject.passHash}")

        return restJTWTProvider.createJWT(role, user.id)
//        return restJTWTProvider.createJWT("ROLE_CICHOPEK", 123)
    }

    companion object {
        private val log = LoggerFactory.getLogger(LoginService::class.java)
    }


}