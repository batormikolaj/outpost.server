package com.kurier.outpost.service

import com.kurier.outpost.OutpostApplication
import com.kurier.outpost.component.DatabaseProvider
import com.kurier.outpost.domain.database.activity.Shift
import com.kurier.outpost.domain.database.activity.ShiftStatus
import com.kurier.outpost.domain.database.activity.ShiftStatusObject
import com.kurier.outpost.domain.database.employee.Carrier
import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.database.parcel.ParcelShiftStatus
import com.kurier.outpost.domain.operatingObject.shift.ParcelAndShiftObject
import com.kurier.outpost.domain.operatingObject.shift.ShiftStatusWorkObject
import com.kurier.outpost.domain.operatingObject.shift.ShiftWithCarriersFlatObject
import com.kurier.outpost.repository.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.Instant
import java.util.stream.Collectors

@Service
class ShiftService {
    @Autowired
    lateinit var databaseProvider: DatabaseProvider

    @Autowired
    lateinit var userRepository: UserRepository

    fun createShift(name: String) {
        val newShift = Shift()
        newShift.name = name

        val listStatus = mutableListOf<ShiftStatusObject>()
        val newShiftStatusObject = ShiftStatusObject()
        newShiftStatusObject.timestamp = Instant.now().epochSecond
        newShiftStatusObject.status = ShiftStatus.CREATED
        newShiftStatusObject.shift = newShift

        databaseProvider.saveShiftStatusObject(newShiftStatusObject)
        listStatus.add(newShiftStatusObject)

        newShift.shiftStatus = listStatus

        databaseProvider.saveShift(newShift)
    }

    fun getShift(idShift: Long): Shift {
        return databaseProvider.getShift(idShift)
    }

    fun getMyShifts(idCarrier: Long): MutableCollection<Shift> {
        val listIdShifts = databaseProvider.getMyShifts(idCarrier)

        val listShifts = arrayListOf<Shift>()

        listIdShifts.forEach {
            val shift = databaseProvider.getShift(it)
            listShifts.add(shift)
        }

        return listShifts
    }

    fun getMyShiftsStatus(idCarrier: Long, status: ShiftStatus): MutableCollection<Shift> {
        val listShifts = databaseProvider.getMyShifts(idCarrier)

        val listShiftsWithStatus = databaseProvider.getShiftsWithStatus(status)

        listShifts.addAll(listShiftsWithStatus)

        val dupa = listShifts.groupingBy { it }.eachCount().filter { it.value > 1 }

        val shifts = arrayListOf<Shift>()
        dupa.forEach {
            shifts.add(databaseProvider.getShift(it.key))
        }

        return shifts
    }

    fun getAllShift(): MutableCollection<Shift> {
        return databaseProvider.getAllShifts()
    }

    fun getCarrierFromShift(idShift: Long): Carrier {
        val idCarrier = databaseProvider.getCarrierFromShift(idShift)

        return databaseProvider.getCarrierById(idCarrier)
    }

    fun connectShiftWithCarriers(idCarriers: MutableCollection<Long>, idShift: Long) {
        val idCarrier = idCarriers.first()
        val carrier = databaseProvider.getCarrierById(idCarrier)

        val newShift = databaseProvider.getShift(idShift)

        databaseProvider.saveShift(newShift)
        carrier.shifts!!.add(newShift)

        databaseProvider.saveUser(carrier)
    }

//    fun createShift(user: User) {
//        val shift=Shift()
//        addShift(shift)
//        userRepository.findById(user.id).
//        databaseProvider.getEmployee(user.id)
//                .filter{it is Carrier }
//                .map {it as Carrier }
//                .map { it.shift!!}
//                .map { it.add(shift) }
//    }

    fun addParcelToShift(tempObject: ParcelAndShiftObject) {
        val shift = databaseProvider.getShift(tempObject.idShift)

        shift.parcels!!.forEach {
            it.parcelShiftStatus = ParcelShiftStatus.NOT_CONNECTED
            databaseProvider.saveParcel(it)
        }
        val tempListParcels = arrayListOf<Parcel>()
        tempObject.idParcels.forEach {
            val parcel = databaseProvider.getParcel(it)
            parcel.parcelShiftStatus = ParcelShiftStatus.CONNECTED
            databaseProvider.saveParcel(parcel)
            tempListParcels.add(parcel)
        }

        databaseProvider.deleteAllParcelsFromShift(tempObject.idShift)

        databaseProvider.saveParcelsToShift(tempObject.idShift, tempObject.idParcels)
    }

    fun changeShiftStatus(idShift: Long, shiftStatus: ShiftStatusWorkObject) {
        val shift = databaseProvider.getShift(idShift)

        val newShiftStatusObject = ShiftStatusObject()
        newShiftStatusObject.timestamp = Instant.now().epochSecond
        newShiftStatusObject.status = shiftStatus.status
        newShiftStatusObject.shift = shift

        databaseProvider.saveShiftStatusObject(newShiftStatusObject)

        shift.shiftStatus!!.add(newShiftStatusObject)

        databaseProvider.saveShift(shift)

    }

    fun getNotConnectedParcels(): MutableCollection<Parcel> {
        return databaseProvider.getNotConnectedParcels()
    }

    fun getCarrierShift(idCarrier: Long): Long {
        return databaseProvider.getCarrierShift(idCarrier)
    }

    companion object {
        val log = LoggerFactory.getLogger(ShiftService::class.java)
    }
}