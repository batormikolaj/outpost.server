package com.kurier.outpost.service

import com.kurier.outpost.component.DatabaseProvider
import com.kurier.outpost.component.DefaultLoginGenerator
import com.kurier.outpost.component.FlatToObjectFactory
import com.kurier.outpost.domain.database.employee.Administrator
import com.kurier.outpost.domain.database.employee.Carrier
import com.kurier.outpost.domain.database.employee.Dispatcher
import com.kurier.outpost.domain.database.general.User
import com.kurier.outpost.domain.operatingObject.employee.AllEmployeesObject
import com.kurier.outpost.domain.operatingObject.employee.EmployeeFlatObject
import com.kurier.outpost.domain.operatingObject.employee.EmployeeWrapper
import com.kurier.outpost.exception.NotFoundException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.ExceptionHandler
import java.util.stream.Collectors

@Service
class EmployeeService {
    val log = LoggerFactory.getLogger(EmployeeService::class.java)!!
    @Autowired
    lateinit var defaultLoginGenerator: DefaultLoginGenerator

    @Autowired
    lateinit var flatToObjectFactory: FlatToObjectFactory

    @Autowired
    lateinit var databaseProvider: DatabaseProvider

    @Autowired
    lateinit var loginService: LoginService

    fun getAllEmployees(): AllEmployeesObject {
        return AllEmployeesObject.Companion.AllEmployeesObjectBuilder()
                .setAdministrators(getAdministrators())
                .setCarriers(getCarriers())
                .setDispatchers(getDispatchers())
                .build()
    }

    fun addEmployee(employeeFlat: EmployeeFlatObject) {
        databaseProvider.saveEmployeeInDb(employeeJSONParser(employeeFlat))
    }

    fun deleteEmployee(id: Long) {
        databaseProvider.deleteEmployee(id)
    }

//    fun deleteAllEmployees(employees: MutableCollection<Carrier>){
//        databaseProvider.deleteAllEmployees(employees)
//    }

    fun employeeJSONParser(employeeFlat: EmployeeFlatObject): EmployeeWrapper {
        val employee = when (employeeFlat.type) {
            "carrier" -> Carrier()
            "dispatcher" -> Dispatcher()
            "administrator" -> Administrator()
            else -> {
                throw NotFoundException("No type of employeeFlat to add: ${employeeFlat.type}")
            }
        }
        val address = flatToObjectFactory.addressFactory(postal_code = employeeFlat.postal_code,
                street = employeeFlat.street,
                street_number = employeeFlat.street_number,
                house_number = employeeFlat.house_number,
                city = employeeFlat.city)


        val person = flatToObjectFactory.personFactory(name = employeeFlat.name,
                surname = employeeFlat.surname,
                telNumber = employeeFlat.telNumber,
                email = employeeFlat.email)

        person.address = address
        val loginObject = defaultLoginGenerator.generateDefaultLoginObject(employeeFlat.name, employeeFlat.surname)
        employee.loginObject = loginObject
        employee.personalia = person
        return EmployeeWrapper(address, person, employee, loginObject)
    }


    fun updateEmployee(id: Long, employeeObject: EmployeeFlatObject) {
        val employee = databaseProvider.getEmployee(id)
        log.info(employee.personalia!!.address!!.id.toString())

        val workObject = employeeJSONParser(employeeObject)
        val address = workObject.address
        val personalia = workObject.person

        databaseProvider.editAddress(employee.personalia!!.address!!.id, address)
        employee.personalia!!.address = address

        databaseProvider.editPerson(personalia, employee.personalia!!.id)
        employee.personalia=personalia

        databaseProvider.employeeDAO.saveEmployee(employee)
    }

    fun getCarriers(): List<Carrier> {
        return databaseProvider.getCarriers()
                .stream()
                .filter { it is Carrier }
                .map { it as Carrier }
                .collect(Collectors.toList())
    }

    fun getDispatchers(): List<Dispatcher> {
        return databaseProvider.getDispatchers()
                .stream()
                .filter { it is Dispatcher }
                .map { it as Dispatcher }
                .collect(Collectors.toList())
    }

    fun getAdministrators(): List<Administrator> {
        return databaseProvider.getAdministrators()
                .stream()
                .filter { it is Administrator }
                .map { it as Administrator }
                .collect(Collectors.toList())
    }

    fun getEmployees(typeOfEmployee: String): List<User> {
        return when (typeOfEmployee) {
            "carriers" -> getCarriers()
            "dispatchers" -> getDispatchers()
            "administrators" -> getAdministrators()
            else -> {
                throw RuntimeException("No type of user found: $typeOfEmployee")
            }
        }
    }

    fun getEmployee(id: Long): User {
        return databaseProvider.getEmployee(id)
    }

    @ExceptionHandler(NotFoundException::class)
    fun exceptionHandler(exception: RuntimeException) {
        log.error(exception.message)
        log.error("Handled ;)")
    }
}