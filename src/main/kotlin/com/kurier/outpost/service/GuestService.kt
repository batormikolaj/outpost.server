package com.kurier.outpost.service

import com.kurier.outpost.domain.database.parcel.ParcelStatusObject
import com.kurier.outpost.repository.ParcelStatusObjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class GuestService {
    @Autowired
    lateinit var parcelStatusObjectRepository: ParcelStatusObjectRepository

    fun checkParcelStatus(parcelNumber: Long): List<ParcelStatusObject> {
        return parcelStatusObjectRepository.findByParcelIdOrderByTimestamp(parcelNumber).toList()
    }

}