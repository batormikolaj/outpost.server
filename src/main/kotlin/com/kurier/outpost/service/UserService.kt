package com.kurier.outpost.service

import com.kurier.outpost.DAO.LoginObjectDAO
import com.kurier.outpost.component.DatabaseProvider
import com.kurier.outpost.component.HashGenerator
import com.kurier.outpost.domain.database.client.Client
import com.kurier.outpost.domain.database.general.LoginObject
import com.kurier.outpost.domain.database.general.Person
import com.kurier.outpost.domain.database.general.User
import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.operatingObject.employee.EmployeeFlatObject
import com.kurier.outpost.domain.operatingObject.parcel.ParcelFlatObject
import com.kurier.outpost.domain.operatingObject.user.LoginPass
import com.kurier.outpost.domain.operatingObject.user.UserFlatObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class UserService {
    @Autowired
    lateinit var loginObjectDAO: LoginObjectDAO

    @Autowired
    lateinit var databaseProvider: DatabaseProvider

    @Autowired
    lateinit var parcelService: ParcelService

    @Autowired
    lateinit var employeeService: EmployeeService

    @Autowired
    lateinit var hashGenerator: HashGenerator

    fun changePass(loginPass: LoginPass) {
        val loginObject = LoginObject()
        loginObject.login = loginPass.login
        loginObject.passHash = hashGenerator.createHash(loginPass.pass)

        loginObjectDAO.changePassHash(loginObject)
    }

    //TODO I know it's redundant, but i cannot come up with anything better
    fun changeLogin(loginPass: LoginPass) {
        val loginObject = LoginObject()
        loginObject.login = loginPass.login
        loginObject.passHash = hashGenerator.createHash(loginPass.pass)

        loginObjectDAO.changeLogin(loginObject)
    }

    //    TODO Do przetestowania
    fun addMyParcel(parcelFlatObject: ParcelFlatObject) {
        parcelService.addParcel(parcelFlatObject)
    }

    fun updateUser(idUser: Long, userFlat: UserFlatObject) {
        val client = databaseProvider.getClientById(idUser)

        client.personalia!!.name = userFlat.name
        client.personalia!!.surname = userFlat.surname
        client.personalia!!.telNumber = userFlat.telNumber
        client.personalia!!.email = userFlat.email

        val address = client.personalia!!.address
        address!!.street = userFlat.street
        address!!.street_number = userFlat.street_number
        address!!.house_number = userFlat.house_number
        address!!.city = userFlat.city
        address!!.postal_code = userFlat.postal_code

        databaseProvider.saveAddress(address)
        databaseProvider.saveUser(client)
    }

    fun getMyParcels(idUser: Long): MutableCollection<Parcel> {
        val listIdParcels = databaseProvider.getMyParcels(idUser)
        val listMyParcels = arrayListOf<Parcel>()
        listIdParcels.forEach {
            val parcel = databaseProvider.getParcel(it)
            listMyParcels.add(parcel)
        }
        return listMyParcels
    }

    fun getUser(idUser: Long): Person {
        return databaseProvider.getClientById(idUser).personalia!!
    }
}