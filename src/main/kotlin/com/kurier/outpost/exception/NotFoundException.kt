package com.kurier.outpost.exception

class NotFoundException(msg: String) : RuntimeException(msg)
