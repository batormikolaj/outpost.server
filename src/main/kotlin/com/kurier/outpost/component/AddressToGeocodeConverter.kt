package com.kurier.outpost.component

import com.fasterxml.jackson.databind.DeserializationFeature
import java.net.URL
import java.net.URLEncoder
import com.fasterxml.jackson.databind.ObjectMapper
import com.kurier.outpost.domain.geocode.GoogleResponse
import org.springframework.stereotype.Component

@Component
class AddressToGeocodeConverter {

    private val APIURL = "http://maps.googleapis.com/maps/api/geocode/json"

    fun convertToGeocode(fullAddress: String): GoogleResponse {
        val url = URL(APIURL + "?address="
                + URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false")
        // Open the Connection
        val conn = url.openConnection()

        val `in` = conn.getInputStream()
        val mapper = ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        val response = mapper.readValue(`in`, GoogleResponse::class.java) as GoogleResponse
        `in`.close()
        return response
    }

}