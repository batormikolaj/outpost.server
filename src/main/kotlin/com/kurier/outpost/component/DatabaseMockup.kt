package com.kurier.outpost.component

import com.kurier.outpost.domain.database.activity.Shift
import com.kurier.outpost.domain.database.activity.ShiftStatus
import com.kurier.outpost.domain.database.activity.ShiftStatusObject
import com.kurier.outpost.domain.database.client.Client
import com.kurier.outpost.domain.database.parcel.*
import com.kurier.outpost.domain.operatingObject.employee.EmployeeFlatObject
import com.kurier.outpost.domain.operatingObject.user.UserFlatObject
import com.kurier.outpost.service.EmployeeService
import com.kurier.outpost.service.LoginService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class DatabaseMockup {
    val log = LoggerFactory.getLogger(DatabaseMockup::class.java)
    @Autowired
    lateinit var employeeService: EmployeeService

    @Autowired
    lateinit var databaseProvider: DatabaseProvider

    @Autowired
    lateinit var flatToObjectFactory: FlatToObjectFactory

    @Autowired
    lateinit var defaultLoginGenerator: DefaultLoginGenerator

    @Autowired
    lateinit var loginService: LoginService

    fun addEmployees(){

        val employee1 = EmployeeFlatObject()
        employee1.type = "carrier"
        employee1.name = "Marek"
        employee1.surname = "Mostkowiak"
        employee1.telNumber = 666123456
        employee1.email = "${employee1.name}${employee1.surname}@outpost.com"

        employee1.city = "kraków"
        employee1.house_number = 123
        employee1.street_number = 456
        employee1.street = "Drabowskiego"
        employee1.postal_code = "33-123"

        val employee2 = EmployeeFlatObject()
        employee2.type = "carrier"
        employee2.name = "Jan"
        employee2.surname = "Drab"
        employee2.telNumber = 235431
        employee2.email = "${employee2.name}${employee2.surname}@outpost.com"

        employee2.city = "Rzeszów"
        employee2.house_number = 1
        employee2.street_number = 488
        employee2.street = "Szrobixa"
        employee2.postal_code = "31-222"

        val employee3 = EmployeeFlatObject()
        employee3.type = "carrier"
        employee3.name = "Marzena"
        employee3.surname = "Grażec"
        employee3.telNumber = 235431432
        employee3.email = "${employee3.name}${employee3.surname}@outpost.com"

        employee3.city = "Wieliczka"
        employee3.house_number = 888
        employee3.street_number = 1
        employee3.street = "Chica"
        employee3.postal_code = "31-333"

        val employee4 = EmployeeFlatObject()
        employee4.type = "dispatcher"
        employee4.name = "Julia"
        employee4.surname = "Grażec"
        employee4.telNumber = 235431432
        employee4.email = "${employee4.name}${employee4.surname}@outpost.com"

        employee4.city = "Wieliczka"
        employee4.house_number = 888
        employee4.street_number = 1
        employee4.street = "Chica"
        employee4.postal_code = "31-333"

        val employee5 = EmployeeFlatObject()
        employee5.type = "dispatcher"
        employee5.name = "Adrian"
        employee5.surname = "Cichopek"
        employee5.telNumber = 665248795
        employee5.email = "${employee5.name}${employee5.surname}@outpost.com"

        employee5.city = "Warszawa"
        employee5.house_number = 8
        employee5.street_number = 5
        employee5.street = "Chicha"
        employee5.postal_code = "31-333"

        val employee6 = EmployeeFlatObject()
        employee6.type = "dispatcher"
        employee6.name = "Krzesimir"
        employee6.surname = "Brzęczyszczykiewicz"
        employee6.telNumber = 758624198
        employee6.email = "${employee6.name}${employee6.surname}@outpost.com"

        employee6.city = "Olsztyn"
        employee6.house_number = 9
        employee6.street_number = 9
        employee6.street = "Głośna"
        employee6.postal_code = "31-322"

        val employee7 = EmployeeFlatObject()
        employee7.type = "administrator"
        employee7.name = "Marek"
        employee7.surname = "Przylepka"
        employee7.telNumber = 562147899
        employee7.email = "${employee7.name}${employee7.surname}@outpost.com"

        employee7.city = "Gdańsk"
        employee7.house_number = 9
        employee7.street_number = 9
        employee7.street = "im. Warunków na pk"
        employee7.postal_code = "31-322"

        databaseProvider.saveEmployeeInDb(employeeService.employeeJSONParser(employee1))
        databaseProvider.saveEmployeeInDb(employeeService.employeeJSONParser(employee2))
        databaseProvider.saveEmployeeInDb(employeeService.employeeJSONParser(employee3))
        databaseProvider.saveEmployeeInDb(employeeService.employeeJSONParser(employee4))
        databaseProvider.saveEmployeeInDb(employeeService.employeeJSONParser(employee5))
        databaseProvider.saveEmployeeInDb(employeeService.employeeJSONParser(employee6))
        databaseProvider.saveEmployeeInDb(employeeService.employeeJSONParser(employee7))
    }

    fun addMockParcel() {
        val address_nadawca = flatToObjectFactory.addressFactory(city = "Krakow", house_number = 55, street_number = 44, street = "Focha", postal_code = "33-213")
        val address_odbiorca = flatToObjectFactory.addressFactory(city = "Krakow", house_number = 3, street_number = 42, street = "Warszawska", postal_code = "33-213")

        val person_nadawca = flatToObjectFactory.personFactory(name = "Jan", surname = "Kowalski", telNumber = 444123123, email = "JanKowalski@outpost.com")
        person_nadawca.address = address_nadawca

        val person_odbiorca = flatToObjectFactory.personFactory(name = "Marzena", surname = "Grażec", telNumber = 987654321, email = "GrażenaMarza@outpost.com")
        person_odbiorca.address = address_odbiorca

        val list_przewozowy = Waybill()
        list_przewozowy.recipient = person_odbiorca
        list_przewozowy.sender = person_nadawca

        val paczka = Parcel()
        paczka.waybill = list_przewozowy
        paczka.gauge = ParcelGauge.A

        val statusObject = ParcelStatusObject()
        statusObject.status = ParcelStatus.PICKED_UP
        statusObject.timestamp = Instant.now().epochSecond
        val lista = mutableListOf<ParcelStatusObject>()
        lista.add(statusObject)
        paczka.status = lista
        paczka.weight = 0.5

        databaseProvider.saveAddress(address_nadawca)
        databaseProvider.saveAddress(address_odbiorca)
        databaseProvider.savePerson(person_nadawca)
        databaseProvider.savePerson(person_odbiorca)
        databaseProvider.saveWaybill(list_przewozowy)
        databaseProvider.saveParcelStatus(statusObject)
        databaseProvider.saveParcel(paczka)
        databaseProvider.saveParcelIdToStatus(paczka.id, statusObject.id)

    }

    fun addMockClient() {
        val client1 = Client()
        val address1 = flatToObjectFactory.addressFactory(
                street = "Goździkowa",
                street_number = 5,
                house_number = 6,
                city = "Gniezdno",
                postal_code = "62-200")
        val person1 = flatToObjectFactory.personFactory(
                name = "Janusz",
                surname = "Nosacz",
                telNumber = 880645685,
                email = "jaNosacz@gmail.com"
        )
        person1.address = address1
        client1.personalia = person1
        val loginObject1 = defaultLoginGenerator.generateDefaultLoginObject(person1.name, person1.surname)
        client1.loginObject = loginObject1

        databaseProvider.saveAddress(address1)
        databaseProvider.savePerson(person1)
        databaseProvider.saveLoginObject(loginObject1)
        databaseProvider.saveClient(client1)

        val client2 = Client()
        val address2 = flatToObjectFactory.addressFactory(
                street = "Mieczykowa",
                street_number = 51,
                house_number = 1,
                city = "Chrzanów",
                postal_code = "32-500")
        val person2 = flatToObjectFactory.personFactory(
                name = "Krystyna",
                surname = "Kmiecik",
                telNumber = 98655412,
                email = "kkmiecik@gmail.com"
        )
        person2.address = address2
        client2.personalia = person2

        val loginObject2 = defaultLoginGenerator.generateDefaultLoginObject(person2.name, person2.surname)
        client2.loginObject = loginObject2

        databaseProvider.saveAddress(address2)
        databaseProvider.savePerson(person2)
        databaseProvider.saveLoginObject(loginObject2)
        databaseProvider.saveClient(client2)
    }

    fun addMockShift() {
        val shift = Shift()
        shift.name = "Zmiana1"
        val statusList = mutableListOf<ShiftStatusObject>()
        val shiftStatus = ShiftStatusObject()
        shiftStatus.timestamp = Instant.now().epochSecond
        shiftStatus.status = ShiftStatus.CREATED

        databaseProvider.saveShiftStatusObject(shiftStatus)
        statusList.add(shiftStatus)
        shift.shiftStatus = statusList
        databaseProvider.saveShift(shift)
    }

    fun mockAddUser() {
        val user1 = UserFlatObject()
        user1.name = "Jan"
        user1.surname = "Niedziela"
        user1.telNumber = 555888999
        user1.email = "jniedziela@gmail.com"
        user1.street = "Różana"
        user1.street_number = 8
        user1.house_number = 5
        user1.city = "Krzeszowice"
        user1.postal_code = "30-200"

        val user2 = UserFlatObject()
        user2.name = "Elżbieta"
        user2.surname = "Nowak"
        user2.telNumber = 444568462
        user2.email = "enowak@gmail.com"
        user2.street = "Bociana"
        user2.street_number = 45
        user2.house_number = 2
        user2.city = "Kraków"
        user2.postal_code = "32-200"

        loginService.addUser(user1)
        loginService.addUser(user2)
    }

}