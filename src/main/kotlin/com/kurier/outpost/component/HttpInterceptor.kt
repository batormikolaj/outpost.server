package com.kurier.outpost.component

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class HttpInterceptor : HandlerInterceptorAdapter() {
    @Autowired
    lateinit var restJTWTProvider: RestJTWTProvider


    override fun preHandle(request: HttpServletRequest?, response: HttpServletResponse?, handler: Any?): Boolean {
        val token = request!!.getHeader("Authorization")
        val extractedToken = token.substring(7)
        restJTWTProvider.verifyToken(extractedToken)
        return true
    }
}