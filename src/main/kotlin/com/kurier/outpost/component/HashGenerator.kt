package com.kurier.outpost.component

import org.springframework.stereotype.Component
import java.security.MessageDigest


@Component
class HashGenerator {
    fun createHash(input: String): String {
        val messageDigest = MessageDigest.getInstance("SHA-256")
        val text = input.toByteArray()
        val digest = messageDigest.digest(text)
        val stringBuffer = StringBuffer()
        for (byte in digest) stringBuffer.append("%02x".format(byte))
        return stringBuffer.toString()
    }
}