package com.kurier.outpost.component

import com.kurier.outpost.DAO.*
import com.kurier.outpost.domain.database.activity.Shift
import com.kurier.outpost.domain.database.activity.ShiftStatus
import com.kurier.outpost.domain.database.activity.ShiftStatusObject
import com.kurier.outpost.domain.database.client.Client
import com.kurier.outpost.domain.database.employee.Administrator
import com.kurier.outpost.domain.database.employee.Carrier
import com.kurier.outpost.domain.database.employee.Dispatcher
import com.kurier.outpost.domain.database.general.Address
import com.kurier.outpost.domain.database.general.LoginObject
import com.kurier.outpost.domain.database.general.Person
import com.kurier.outpost.domain.database.general.User
import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.database.parcel.ParcelStatus
import com.kurier.outpost.domain.database.parcel.ParcelStatusObject
import com.kurier.outpost.domain.database.parcel.Waybill
import com.kurier.outpost.domain.operatingObject.employee.EmployeeWrapper
import com.kurier.outpost.domain.operatingObject.parcel.ParcelFlatObject
import com.kurier.outpost.domain.operatingObject.shift.ShiftWithCarriersFlatObject
import com.kurier.outpost.domain.operatingObject.user.LoginPass
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Component
@Transactional
class DatabaseProvider {
    @Autowired
    lateinit var employeeDAO: EmployeeDAO

    @Autowired
    lateinit var addressDAO: AddressDAO

    @Autowired
    lateinit var personDAO: PersonDAO

    @Autowired
    lateinit var parcelDAO: ParcelDAO

    @Autowired
    lateinit var waybillDAO: WaybillDAO

    @Autowired
    lateinit var clientDAO: ClientDAO

    @Autowired
    lateinit var shiftDAO: ShiftDAO

    @Autowired
    lateinit var parcelStatusObjectDAO: ParcelStatusObjectDAO

    @Autowired
    lateinit var shiftStatusObjectDAO: ShiftStatusObjectDAO

    @Autowired
    lateinit var loginObjectDAO: LoginObjectDAO

    //ADDRESS
    fun saveAddress(address: Address) {
        addressDAO.saveAddress(address)
    }

    //PERSON
    fun savePerson(person: Person) {
        personDAO.savePerson(person)
    }

    //EMPLOYEE
    fun getEmployee(id: Long): User {
        return employeeDAO.getEmployee(id)
    }

    fun getCarriers(): MutableList<Carrier> {
        return employeeDAO.findCarriers()
    }

    fun getDispatchers(): MutableList<Dispatcher> {
        return employeeDAO.findDispatchers()
    }

    fun getAdministrators(): MutableList<Administrator> {
        return employeeDAO.findAdministrators()
    }

    fun editAddress(idAddressExisting: Long, address: Address) {
        addressDAO.updateAddress(idAddressExisting, address)
    }

    fun editPerson(person: Person, address_id: Long) {
        personDAO.updatePerson(person, address_id)
    }

    fun saveEmployeeInDb(address: Address, person: Person, loginObject: LoginObject, user: User) {
        addressDAO.saveAddress(address)
        personDAO.savePerson(person)
        loginObjectDAO.saveLoginObject(loginObject)
        employeeDAO.saveEmployee(user)
    }

    fun saveEmployeeInDb(address: Address, person: Person, user: User) {
        addressDAO.saveAddress(address)
        personDAO.savePerson(person)
        employeeDAO.saveEmployee(user)
    }

    fun saveEmployeeInDb(employeeWrapper: EmployeeWrapper) {
        addressDAO.saveAddress(employeeWrapper.address)
        personDAO.savePerson(employeeWrapper.person)
        loginObjectDAO.saveLoginObject(employeeWrapper.loginObject)
        employeeDAO.saveEmployee(employeeWrapper.user)
        employeeWrapper.loginObject.owner = employeeWrapper.user
        loginObjectDAO.saveLoginObject(employeeWrapper.loginObject)
    }

    fun saveUser(carrier: User) {
        employeeDAO.saveEmployee(carrier)
    }

    fun deleteEmployee(id: Long) {
        employeeDAO.deleteEmployee(id)
    }

//    fun deleteAllCarriersFromShift(shiftId: Long) {
//        shiftDAO.deleteAllCarriersFromShiftById(shiftId)
//    }

    fun deleteAllParcelsFromShift(shiftId: Long) {
        shiftDAO.deleteAllParcelsFromShiftById(shiftId)
    }

    fun saveParcelsToShift(idShift: Long, idParcels: MutableCollection<Long>) {
        shiftDAO.saveParcelsToShift(idShift, idParcels)
    }

    fun getCarrierById(idCarrier: Long): Carrier {
        return employeeDAO.getCarrierById(idCarrier)
    }

    //PARCEL
    fun changeStatusOfParcel(id: Long, status: ParcelStatus) {
        parcelDAO.updateParcelStatus(id, status)
    }

    fun getAllParcels(): List<Parcel> {
        return parcelDAO.getAllParcels()
    }

    fun saveParcel(parcel: Parcel) {
        parcelDAO.saveParcel(parcel)
    }

    fun getParcel(idParcel: Long): Parcel {
        return parcelDAO.getParcel(idParcel)
    }

    fun getAllStatusParcel(idParcel: Long): MutableCollection<ParcelStatusObject> {
        return parcelDAO.getAllStatus(idParcel)
    }

    fun getParcelsByStatus(parcelStatus: ParcelStatus): MutableCollection<Long> {
        return parcelDAO.getParcelsByStatus(parcelStatus)
    }

    fun getNotConnectedParcels(): MutableCollection<Parcel> {
        return parcelDAO.getNotConnectedParcels()
    }

    fun findParcelByShiftI(idShift: Long): MutableCollection<Parcel>? {
        return parcelDAO.findParcelById(idShift)
    }

    //WAYBILL
    fun saveWaybill(waybill: Waybill) {
        waybillDAO.saveWaybill(waybill)
    }

    //CLIENT
    fun getClientById(id: Long): Client {
        return clientDAO.getClient(id)
    }

    fun saveClient(client: Client) {
        clientDAO.addClient(client)
    }

    //PARCEL STATUS OBJECT
    fun saveParcelStatus(parcelStatusObject: ParcelStatusObject) {
        parcelStatusObjectDAO.saveStatusObject(parcelStatusObject)
    }

    fun getParcelStatus(id: Long): ParcelStatusObject {
        return parcelStatusObjectDAO.getRecentStatus(id)
    }

    fun saveParcelIdToStatus(parcel_id: Long, status_id: Long) {
        parcelStatusObjectDAO.saveParcelId(parcel_id, status_id)
    }

    //    TODO XD Opcional List
    fun getParcelHistory(parcelId: Long): MutableCollection<ParcelStatusObject> {
        return parcelStatusObjectDAO.findById(parcelId)
    }

    //LOGIN OBJECT
    fun saveLoginObject(loginObject: LoginObject) {
        loginObjectDAO.saveLoginObject(loginObject)
    }

    fun editLoginObject(loginObject: LoginObject) {
        loginObjectDAO.editObject(loginObject)
    }

    fun getLogin(login: String): LoginObject {
        return loginObjectDAO.getLogin(login)
    }

    fun exists(login: String): Boolean {
        return loginObjectDAO.chechIfExists(login)
    }

    //SHIFT
    fun saveShift(shift: Shift) {
        shiftDAO.addShift(shift)
    }

    fun saveShiftStatusObject(shiftStatusObject: ShiftStatusObject) {
        shiftStatusObjectDAO.addShiftStatusObject(shiftStatusObject)
    }

    fun getAllShifts(): MutableCollection<Shift> {
        return shiftDAO.getAllShifts()
    }

    fun getCarrierFromShift(idShift: Long): Long {
        return shiftDAO.getCarrierFromShift(idShift)
    }

    fun getShift(idShift: Long): Shift {
        return shiftDAO.getShift(idShift)
    }

    fun getMyShifts(idCarrier: Long): MutableCollection<Long> {
        return shiftDAO.getMyShifts(idCarrier)
    }

    fun getShiftsWithStatus(status: ShiftStatus): MutableCollection<Long> {
        val statusInt = status.ordinal
        return shiftDAO.getShiftsWithStatus(statusInt)
    }

    fun getCarrierShift(idCarrier: Long): Long {
        return shiftDAO.getCarrierShift(idCarrier)
    }


    //USER
    fun getMyParcels(idUser: Long): MutableCollection<Long> {
        return clientDAO.getMyParcels(idUser)
    }

}