package com.kurier.outpost.component

import com.kurier.outpost.domain.database.general.Address
import com.kurier.outpost.domain.database.general.Person
import org.springframework.stereotype.Component

@Component
class FlatToObjectFactory {

    fun addressFactory(street: String, street_number: Int,
                       house_number: Int, city: String,
                       postal_code: String): Address {
        val address = Address()
        address.street = street
        address.street_number = street_number
        address.house_number = house_number
        address.city = city
        address.postal_code = postal_code
        return address
    }

    fun personFactory(name: String, surname: String, telNumber: Int, email: String): Person {
        val person = Person()
        person.name = name
        person.surname = surname
        person.telNumber = telNumber
        person.email = email
        return person
    }
}