package com.kurier.outpost.component

import com.kurier.outpost.domain.operatingObject.parcel.ParcelChangeStatusObject
import com.kurier.outpost.domain.operatingObject.shift.ShiftStatusChange
import com.kurier.outpost.domain.operatingObject.shift.ShiftStatusWorkObject
import com.kurier.outpost.service.ParcelService
import com.kurier.outpost.service.ShiftService
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitHandler
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
@RabbitListener(queues = ["android"])
class RabbitConnector {
    val log = LoggerFactory.getLogger(RabbitConnector::class.java)

    @Autowired
    lateinit var parcelService: ParcelService
    @Autowired
    lateinit var shiftService: ShiftService

    @RabbitHandler
    fun rabbitReceiver(text:String){
        log.info("Received: $text")
    }

    @RabbitHandler
    fun changeStatus(statusObj: ParcelChangeStatusObject) {
        log.info("Received: ${statusObj.id} ${statusObj.status}")
        parcelService.changeStatusOfParcel(statusObj.id, statusObj.status!!)
    }

    @RabbitHandler
    fun changeShiftStatus(status: ShiftStatusChange) {
        log.info("Received: ${status.idShift} ${status.status}")
        val statusObj = ShiftStatusWorkObject()
        statusObj.status = status.status!!
        shiftService.changeShiftStatus(status.idShift, statusObj)
    }
}