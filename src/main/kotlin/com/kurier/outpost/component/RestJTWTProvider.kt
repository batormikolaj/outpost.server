package com.kurier.outpost.component

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.springframework.stereotype.Component
import java.io.UnsupportedEncodingException
import java.security.Permission

@Component
class RestJTWTProvider {

    fun createJWT(role: String, userId: Long): String? {
        var token: String? = null
        try {
            val algorithm = Algorithm.HMAC256("secret")
            token = JWT.create()
                    .withClaim("roles", role)
                    .withClaim("user_id", userId)
                    .sign(algorithm)
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        return token
    }

    fun verifyToken(token: String) {
        println("Kupa")
    }
}