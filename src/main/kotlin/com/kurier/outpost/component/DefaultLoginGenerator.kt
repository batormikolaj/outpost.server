package com.kurier.outpost.component

import com.kurier.outpost.domain.database.general.LoginObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class DefaultLoginGenerator {
    @Autowired
    lateinit var hashGenerator: HashGenerator

    fun generateDefaultLoginObject(name: String, surname: String): LoginObject {
        val loginObject = LoginObject()
        loginObject.login = "$name+$surname"
        loginObject.passHash = hashGenerator.createHash("$name$surname")
        return loginObject
    }
}