package com.kurier.outpost.repository

import com.kurier.outpost.domain.database.general.Person
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface PersonRepository:JpaRepository<Person,Long> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE PERSON P SET p.email = ?2, p.name = ?3, p.surname = ?4, p.tel_Number = ?5, p.address_id = ?6 where p.id = ?1", nativeQuery = true)
    fun updatePerson(id: Long, email: String, name: String, surname: String, telNumber: Int, address_id: Long)
}