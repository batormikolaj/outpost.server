package com.kurier.outpost.repository

import com.kurier.outpost.domain.database.activity.ShiftStatusObject
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ShiftStatusObjectRepository: JpaRepository<ShiftStatusObject, Long> {
}