package com.kurier.outpost.repository

import com.kurier.outpost.domain.database.activity.Shift
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface ShiftRepository : JpaRepository<Shift, Long> {

//    @Modifying
//    @Query(value = "DELETE FROM SHIFT WHERE SHIFT_ID=?1", nativeQuery = true)
//    fun deleteCarrierFromShift(idShift: Long)

    @Modifying
    @Query(value = "DELETE FROM SHIFT_PARCELS WHERE SHIFT_ID=?1", nativeQuery = true)
    fun deleteParcelsFromShift(idShift: Long)

    @Modifying
    @Query(value = "INSERT INTO SHIFT_PARCELS (SHIFT_ID, PARCELS_ID) VALUES (?1, ?2)", nativeQuery = true)
    fun saveParcelsToShift(idShift: Long, idParcel: Long)

    @Query(value = "SELECT SHIFTS_ID FROM CARRIER_SHIFTS  WHERE CARRIER_ID=?1", nativeQuery = true)
    fun findByMyId(idCarrier: Long): MutableCollection<Long>

    //    @Query(value = "SELECT CARRIERS_ID FROM SHIFT_CARRIERS WHERE SHIFT_ID=?1", nativeQuery = true)
    @Query(value = "SELECT CARRIER_ID FROM CARRIER_SHIFTS WHERE SHIFTS_ID=?1", nativeQuery = true)
    fun getCarrierFromShift(idShift: Long): Long

    //    SELECT SHIFT_ID FROM SHIFT_STATUS_OBJECT p1 WHERE timestamp = (SELECT MAX(timestamp) FROM SHIFT_STATUS_OBJECT p2 WHERE p1.SHIFT_ID = p2.SHIFT_ID) AND p1.STATUS=2 GROUP BY SHIFT_ID
    @Query(value = "SELECT SHIFT_ID FROM SHIFT_STATUS_OBJECT p1 WHERE timestamp = (SELECT MAX(timestamp) FROM SHIFT_STATUS_OBJECT p2 WHERE p1.SHIFT_ID = p2.SHIFT_ID) AND p1.STATUS=?1 GROUP BY SHIFT_ID", nativeQuery = true)
    fun getShiftsWithStatus(statusInt: Int): MutableCollection<Long>

    @Query(value = "SELECT shifts_id FROM CARRIER_SHIFTS WHERE CARRIER_ID = ?1 AND SHIFTS_ID = (\n" +
            "\tSELECT shift_id from SHIFT_SHIFT_STATUS WHERE SHIFT_STATUS_ID = (\n" +
            "\t\tSELECT Id FROM SHIFT_STATUS_OBJECT WHERE \"TIMESTAMP\" = \n" +
            "\t\t\t(SELECT MAX(\"TIMESTAMP\") FROM SHIFT_STATUS_OBJECT WHERE STATUS = 0)\n" +
            "\t\t)\n" +
            ")", nativeQuery = true)
    fun findParcelsFromShiftByCarrier(idCarrier: Long): Long
}