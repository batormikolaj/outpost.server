package com.kurier.outpost.repository

import com.kurier.outpost.domain.database.employee.Administrator
import com.kurier.outpost.domain.database.employee.Carrier
import com.kurier.outpost.domain.database.employee.Dispatcher
import com.kurier.outpost.domain.database.general.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : JpaRepository<User, Long> {

    @Query(value="SELECT * FROM carrier", nativeQuery = true)
    fun findCarriers(): MutableList<Carrier>

    @Query(value="SELECT * FROM dispatcher", nativeQuery = true)
    fun findDispatchers(): MutableList<Dispatcher>

    @Query(value="SELECT * FROM administrator", nativeQuery = true)
    fun findAdministrators(): MutableList<Administrator>

    override fun findById(id: Long?): Optional<User>

    @Query(value="SELECT * FROM carrier WHERE ID=?1", nativeQuery = true)
    fun findCarrierById(id: Long): Carrier
}