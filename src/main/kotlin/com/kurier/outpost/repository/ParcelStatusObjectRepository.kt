package com.kurier.outpost.repository


import com.kurier.outpost.domain.database.parcel.ParcelStatus
import com.kurier.outpost.domain.database.parcel.ParcelStatusObject
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ParcelStatusObjectRepository : JpaRepository<ParcelStatusObject, Long> {
    fun findByParcelIdOrderByTimestamp(id: Long): MutableCollection<ParcelStatusObject>
    fun findByParcelIdOrderByTimestampDesc(id: Long): ParcelStatusObject

    @Modifying
    @Query("UPDATE Parcel_Status_Object p SET p.parcel_id = ?1 where p.id = ?2", nativeQuery = true)
    fun updateParcelId(parcel_id: Long, status_id: Long)

    @Query("SELECT * FROM PARCEL_STATUS_OBJECT WHERE PARCEL_ID = ?1", nativeQuery = true)
    fun findAllById(parcel_id: Long): MutableCollection<ParcelStatusObject>

    fun findByStatus(status: ParcelStatus): List<ParcelStatusObject>
}