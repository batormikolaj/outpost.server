package com.kurier.outpost.repository

import com.kurier.outpost.domain.database.general.Address
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface AddressRepository :JpaRepository<Address,Long>{
    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE ADDRESS a SET a.city = ?2, a.house_number = ?3, a.postal_code = ?4, a.street = ?5, a.street_number = ?6  where a.id = ?1", nativeQuery = true)
    fun updateAddress(id: Long, city: String, house_number: Int, postal_code: String, street: String, street_number: Int)
}