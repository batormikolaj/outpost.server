package com.kurier.outpost.repository

import com.kurier.outpost.domain.database.general.LoginObject
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional

@Repository
interface LoginRepository : JpaRepository<LoginObject, String> {

    fun findByLogin(login: String): Optional<LoginObject>

    @Modifying
    @Transactional
    @Query("update LOGIN_OBJECT lo set lo.PASS_HASH = ?1 where lo.LOGIN = ?2", nativeQuery = true)
    fun setPassHash(passHash: String, login: String)

    @Modifying
    @Transactional
    @Query("update LOGIN_OBJECT lo set lo.LOGIN = ?1 where lo.PASS_HASH = ?2", nativeQuery = true)
    fun setLogin(login: String, passHash: String)

    fun existsByLogin(login: String): Boolean
}