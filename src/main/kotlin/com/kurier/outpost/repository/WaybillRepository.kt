package com.kurier.outpost.repository

import com.kurier.outpost.domain.database.parcel.Waybill
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface WaybillRepository : JpaRepository<Waybill, Long> {
}