package com.kurier.outpost.repository

import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.database.parcel.ParcelStatus
import com.kurier.outpost.domain.database.parcel.ParcelShiftStatus
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ParcelRepository : JpaRepository<Parcel, Long> {
    @Query(value = "SELECT PARCEL_ID FROM PARCEL_STATUS_OBJECT p1 WHERE timestamp = (SELECT MAX(timestamp) FROM PARCEL_STATUS_OBJECT p2 WHERE p1.PARCEL_ID = p2.PARCEL_ID) AND p1.STATUS=?1 GROUP BY PARCEL_ID;", nativeQuery = true)
    fun getParcelsByStatus(status: Int) : MutableCollection<Long>

    fun findByParcelShiftStatus(parcelShiftStatus: ParcelShiftStatus): MutableCollection<Parcel>

}