package com.kurier.outpost.repository

import com.kurier.outpost.domain.database.client.Client
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ClientRepository : JpaRepository<Client, Long> {

    @Query(value = "SELECT HISTORY_ID FROM CLIENT_HISTORY WHERE CLIENT_ID=?1", nativeQuery = true)
    fun findMyParcels(idClient: Long): MutableCollection<Long>
}