package com.kurier.outpost.configuration

import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.connection.RabbitConnectionFactoryBean
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.amqp.support.converter.SimpleMessageConverter
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration
import org.springframework.boot.autoconfigure.amqp.RabbitProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableRabbit
class RabbitConnection {
    //    @Bean
//    fun rabbitConnectionFactory(configuration: RabbitProperties): RabbitProperties {
//        val factoryBean= RabbitConnectionFactoryBean()
//        factoryBean.setHost(configuration.host)
////        factoryBean.setUsername(configuration.username)
////        factoryBean.setPassword(configuration.password)
//        factoryBean.setPort(configuration.port)
//        return factoryBean
//    }
    @Bean
    fun rabbitTemplate(connectionFactory: ConnectionFactory): RabbitTemplate {
        val rabbitTemplate = RabbitTemplate(connectionFactory)
        rabbitTemplate.messageConverter = simpleMessageConverter()
        rabbitTemplate.messageConverter = jsonMessageConverter()
        return rabbitTemplate
    }

    @Bean
    fun jsonMessageConverter(): MessageConverter {
        return Jackson2JsonMessageConverter()
    }

    @Bean
    fun simpleMessageConverter(): SimpleMessageConverter {
        return SimpleMessageConverter()
    }
}