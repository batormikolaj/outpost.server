//package com.kurier.outpost.configuration
//
//import org.springframework.context.annotation.Configuration
//import org.springframework.security.config.annotation.web.builders.HttpSecurity
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
//
//@Configuration
//@EnableWebSecurity
//class SecurityConfig : WebSecurityConfigurerAdapter() {
//
//    override fun configure(http: HttpSecurity?) {
//        http!!.authorizeRequests()
//                .antMatchers("/guest/**").permitAll()
//                .antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
//                .antMatchers("/carrier/**").access("hasRole('ROLE_CARRIER')")
//                .antMatchers("/user/**").access("hasRole('ROLE_USER')")
//                .antMatchers("/dispatcher/**").access("hasRole('ROLE_DISPATCHER')")
//
//                .csrf().disable()
//
//    }
//}