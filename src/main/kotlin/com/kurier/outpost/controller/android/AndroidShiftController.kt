package com.kurier.outpost.controller.android

import com.kurier.outpost.domain.database.activity.Shift
import com.kurier.outpost.domain.database.activity.ShiftStatus
import com.kurier.outpost.domain.database.activity.ShiftStatusObject
import com.kurier.outpost.domain.database.general.User
import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.operatingObject.shift.ShiftStatusWorkObject
import com.kurier.outpost.service.ParcelService
import com.kurier.outpost.service.ShiftService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/android/shift")
class AndroidShiftController {
    @Autowired
    lateinit var shiftService: ShiftService

    @Autowired
    lateinit var parcelService: ParcelService

//    //Wyświetlanie paczek dla idKurier/idZmiana
//    @RequestMapping(method = [RequestMethod.GET], path = ["/{idShift}/getShift"])
//    fun getShift(@PathVariable("idShift") idShift: Long): Shift {
//        return shiftService.getShift(idShift)
//    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/{idCarrier}/getShift"])
    fun getShift(@PathVariable("idCarrier") idCarrier: Long): Long {
        return shiftService.getCarrierShift(idCarrier)
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/{idShift}/getParcelsFromShift"])
    fun getParcelsFromShift(@PathVariable("idShift") idShift: Long): MutableCollection<Parcel>? {
        return parcelService.findByShiftId(idShift)
    }

//    @RequestMapping(method = [RequestMethod.GET], path = ["/{idCarrier}/getMyShift"])
//    fun getMyShifts(@PathVariable("idCarrier") idCarrier: Long): MutableCollection<Shift> {
//        return shiftService.getMyShifts(idCarrier)
//    }
    @RequestMapping(method = [RequestMethod.POST], path = ["/{idShift}/changeStatus"])
    fun changeShiftStatus(@RequestBody shiftStatus: ShiftStatusWorkObject, @PathVariable("idShift") idShift: Long) {
        shiftService.changeShiftStatus(idShift, shiftStatus)
    }
}