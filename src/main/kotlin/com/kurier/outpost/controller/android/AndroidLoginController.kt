package com.kurier.outpost.controller.android

import com.kurier.outpost.domain.operatingObject.user.LoginPass
import com.kurier.outpost.service.LoginService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/android")
class AndroidLoginController {

    @Autowired
    lateinit var loginService: LoginService

    @RequestMapping(method = [RequestMethod.POST], path = ["/login"])
    fun login(@RequestBody user: LoginPass): String? {
        return loginService.login(user.login, user.pass)
    }

}