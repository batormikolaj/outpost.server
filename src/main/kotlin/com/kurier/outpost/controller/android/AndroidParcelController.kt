package com.kurier.outpost.controller.android

import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.operatingObject.parcel.ParcelAddressFlatObject
import com.kurier.outpost.domain.operatingObject.parcel.ParcelChangeStatusObject
import com.kurier.outpost.service.ParcelService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/android/parcel")
class AndroidParcelController {

    @Autowired
    lateinit var parcelService: ParcelService

    @RequestMapping(method = [RequestMethod.GET], path = ["/{id}"])
    fun getParcelInfo(@PathVariable("id") id: Long): Parcel {
        return parcelService.getParcel(id)
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/changeStatus"])
    fun changeStatus(@RequestBody request: ParcelChangeStatusObject) {
        parcelService.changeStatusOfParcel(request.id, request.status!!)
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/getGeocode"])
    fun createGeocode(@RequestBody address: ParcelAddressFlatObject): String {
        return parcelService.getGeocode(address)
    }

}