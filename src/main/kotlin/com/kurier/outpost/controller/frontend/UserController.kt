package com.kurier.outpost.controller.frontend

import com.kurier.outpost.domain.database.client.Client
import com.kurier.outpost.domain.database.general.Person
import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.database.parcel.ParcelStatusObject
import com.kurier.outpost.domain.operatingObject.parcel.ParcelFlatObject
import com.kurier.outpost.domain.operatingObject.user.LoginPass
import com.kurier.outpost.domain.operatingObject.user.UserFlatObject
import com.kurier.outpost.service.ParcelService
import com.kurier.outpost.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
@RequestMapping("/user")
class UserController {
    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var parcelService: ParcelService

    //TODO: Add sender ID to ParcelFlatObject
    @RequestMapping(value = ["/addParcel"], method = [RequestMethod.POST])
    fun sendParcel(@RequestBody parcelFlatObject: ParcelFlatObject) {
        parcelService.addParcel(parcelFlatObject)
    }

    @RequestMapping(value = ["/{id}/myParcels"], method = [RequestMethod.GET])
    fun getMyParcels(@PathVariable("id") idUser: Long): MutableCollection<Parcel> {
        return userService.getMyParcels(idUser)
    }

    @RequestMapping(value = ["/{id}/editUser"], method = [RequestMethod.POST])
    fun editUser(@PathVariable("id") idUser: Long, @RequestBody userFlat: UserFlatObject) {
        userService.updateUser(idUser, userFlat)
    }

    @RequestMapping(value = ["/{id}/details"], method = [RequestMethod.GET])
    fun getUser(@PathVariable("id") idUser: Long): Person {
        return userService.getUser(idUser)
    }

    @RequestMapping(value = ["/parcelHistory/{id}"], method = [RequestMethod.GET])
    fun parcelHistory(@PathVariable("id") parcelId: Long): MutableCollection<ParcelStatusObject> {
        return parcelService.getParcelHistory(parcelId)
    }

    @RequestMapping(value = ["/getParcelStatus/{id}"], method = [RequestMethod.GET])
    fun getParcelStatus(id: Long): ParcelStatusObject {
        return parcelService.getParcelStatus(id)
    }

    @RequestMapping(value = ["/addPredefinedClient"], method = [RequestMethod.POST])
    fun addPredefinedClient() {
    }

    @RequestMapping(value = ["/changePassword"], method = [RequestMethod.POST])
    fun changePass(@RequestBody loginPass: LoginPass) {
        userService.changePass(loginPass)
    }

    @RequestMapping(value = ["/allParcels"], method = [RequestMethod.GET])
    fun getAllParcels(): List<Parcel> {
        return parcelService.getAllParcels()
    }
}