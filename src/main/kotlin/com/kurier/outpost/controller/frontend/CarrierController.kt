package com.kurier.outpost.controller.frontend

import com.kurier.outpost.domain.database.activity.Shift
import com.kurier.outpost.domain.database.activity.ShiftStatus
import com.kurier.outpost.domain.database.activity.ShiftStatusObject
import com.kurier.outpost.domain.database.employee.Carrier
import com.kurier.outpost.domain.operatingObject.shift.ShiftStatusWorkObject
import com.kurier.outpost.service.ShiftService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
@RequestMapping("/carrier")
class CarrierController {

    @Autowired
    lateinit var shiftService: ShiftService

    @RequestMapping(method = [RequestMethod.GET], path = ["/{idShift}/getShift"])
    fun getShift(@PathVariable("idShift") idShift: Long): Shift {
        return shiftService.getShift(idShift)
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/{idCarrier}/getMyShift"])
    fun getMyShifts(@PathVariable("idCarrier") idCarrier: Long): MutableCollection<Shift> {
        return shiftService.getMyShifts(idCarrier)
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/{idCarrier}/getMyShift/{status}"])
    fun getMyShiftsStatus(@PathVariable("idCarrier") idCarrier: Long, @PathVariable("status") status: ShiftStatus): MutableCollection<Shift> {
        return shiftService.getMyShiftsStatus(idCarrier, status)
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/{idShift}/changeStatus"])
    fun changeShiftStatus(@RequestBody shiftStatus: ShiftStatusWorkObject, @PathVariable("idShift") idShift: Long) {
        shiftService.changeShiftStatus(idShift, shiftStatus)
    }

//    @RequestMapping(method = [RequestMethod.GET], path = ["/getCarrier"])
//    fun getCarrier(@RequestBody idCarrier: Long): Carrier {
//        return carrierService.getCarrier(idCarrier)
//    }
//
//    @RequestMapping(method = [RequestMethod.POST], path = ["/addCarrier"])
//    fun addCarrier(@RequestBody idCarrier: Long): Carrier {
//        return carrierService.getCarrier(idCarrier)
//    }
}