package com.kurier.outpost.controller.frontend

import com.kurier.outpost.component.DatabaseMockup
import com.kurier.outpost.domain.database.general.User
import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.operatingObject.employee.AllEmployeesObject
import com.kurier.outpost.domain.operatingObject.employee.EmployeeFlatObject
import com.kurier.outpost.domain.operatingObject.parcel.ParcelChangeStatusObject
import com.kurier.outpost.domain.operatingObject.parcel.ParcelFlatObject
import com.kurier.outpost.service.EmployeeService
import com.kurier.outpost.service.ParcelService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
@RequestMapping("/admin")
class AdminController {

    @Autowired
    lateinit var employeeService: EmployeeService

    @Autowired
    lateinit var databaseMockUp: DatabaseMockup

    @Autowired
    lateinit var parcelService: ParcelService

    //EMPLOYEES
    @RequestMapping(method = [RequestMethod.GET], path = ["/{type}"])
    fun getEmployees(@PathVariable("type") typeOfEmployee: String): List<User> {
        return employeeService.getEmployees(typeOfEmployee)
    }

    @RequestMapping(path = ["/all"])
    fun getAllEmployees(): AllEmployeesObject {
        return employeeService.getAllEmployees()
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/addEmployee"])
    fun addEmployee(@RequestBody employeeFlatObject: EmployeeFlatObject) {
        employeeService.addEmployee(employeeFlatObject)
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/{id}/editEmployee"])
    fun editEmployee(@PathVariable("id") id: Long, @RequestBody employeeObject: EmployeeFlatObject) {
        employeeService.updateEmployee(id, employeeObject)
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/deleteEmployee"])
    fun deleteEmployee(@RequestBody id: Long) {
        employeeService.deleteEmployee(id)
    }

    @RequestMapping("/add")
    fun addEmployees() {
        databaseMockUp.addEmployees()
        databaseMockUp.mockAddUser()
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/getEmployee/{id}"])
    fun getEmployee(@PathVariable("id") id: Long): User {
        return employeeService.getEmployee(id)
    }

    //PARCELS
    @RequestMapping(method = [RequestMethod.POST], path = ["/changeStatus"])
    fun changeStatus(@RequestBody request: ParcelChangeStatusObject) {
        parcelService.changeStatusOfParcel(request.id, request.status!!)
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/getParcels"])
    fun getParcels(): List<Parcel> {
        return parcelService.getAllParcels()
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/addMockParcel"])
    fun addParcel() {
        databaseMockUp.addMockParcel()
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/addParcel"])
    fun addParcel(@RequestBody parcelFlatObject: ParcelFlatObject) {
        parcelService.addParcel(parcelFlatObject)
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/changePrices"])
    fun changePrices() {
    }


}