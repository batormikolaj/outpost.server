package com.kurier.outpost.controller.frontend

import com.kurier.outpost.DAO.ParcelDAO
import com.kurier.outpost.domain.database.activity.Shift
import com.kurier.outpost.domain.database.employee.Carrier
import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.database.parcel.ParcelStatus
import com.kurier.outpost.domain.operatingObject.shift.ParcelAndShiftObject
import com.kurier.outpost.domain.operatingObject.shift.ShiftWithCarriersFlatObject
import com.kurier.outpost.domain.operatingObject.shift.SimpleShiftObject
import com.kurier.outpost.service.EmployeeService
import com.kurier.outpost.service.ParcelService
import com.kurier.outpost.service.ShiftService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
@RequestMapping("/dispatcher")
class DispatcherController {
    @Autowired
    lateinit var shiftService: ShiftService

    @Autowired
    lateinit var employeeService: EmployeeService

    @Autowired
    lateinit var parcelService: ParcelService

    @Autowired
    lateinit var parcelDAO: ParcelDAO

    @RequestMapping(method = [RequestMethod.POST], path = ["/createShift"])
    fun createShift(@RequestBody simpleShiftObject: SimpleShiftObject) {
        shiftService.createShift(simpleShiftObject.name)
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/{idShift}/getShift"])
    fun getShift(@PathVariable("idShift") idShift: Long): Shift {
        return shiftService.getShift(idShift)
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/getAllShifts"])
    fun getAllShifts(): MutableCollection<Shift> {
        return shiftService.getAllShift()
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/{idShift}/getCarrierFromShift"])
    fun getAllShifts(@PathVariable("idShift") idShift: Long): Carrier {
        return shiftService.getCarrierFromShift(idShift)
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/getNotConnectedParcels"])
    fun getNotConnectedParcels(): MutableCollection<Parcel> {
        return shiftService.getNotConnectedParcels()
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/connectCarrierWithShifts"])
    fun connectCarrierWithShifts(@RequestBody objecte: ShiftWithCarriersFlatObject) {
        shiftService.connectShiftWithCarriers(objecte.idCarriers, objecte.idShift)
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["/getParcelsByStatus/{status}"])
    fun getParcelsByStatus(@PathVariable status: ParcelStatus): MutableCollection<Parcel> {
        return parcelService.getParcelsByStatus(status)
    }

    //    @RequestMapping("/addParcelToShift")
//    fun addParcelToShift(@RequestBody parcel: Parcel,@RequestBody shift: Shift){
//        shiftService.addParcelToShift(parcel,shift)
//    }
    @RequestMapping(method = [RequestMethod.POST], path = ["/addParcelToShift"])
    fun addParcelToShift(@RequestBody tempObject: ParcelAndShiftObject) {
        shiftService.addParcelToShift(tempObject)
    }
}