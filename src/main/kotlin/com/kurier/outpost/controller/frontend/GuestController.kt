package com.kurier.outpost.controller.frontend

import com.kurier.outpost.domain.database.parcel.ParcelStatusObject
import com.kurier.outpost.domain.operatingObject.user.LoginPass
import com.kurier.outpost.domain.operatingObject.user.UserFlatObject
import com.kurier.outpost.log
import com.kurier.outpost.service.GuestService
import com.kurier.outpost.service.LoginService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = ["http://localhost:3000"])
@RequestMapping("/guest")
class GuestController {
    @Autowired
    lateinit var guestService: GuestService

    @Autowired
    lateinit var loginService: LoginService

    @RequestMapping(value = ["/signUp"], method = [RequestMethod.POST])
    fun registrarUser(@RequestBody flatUser: UserFlatObject) {
        loginService.addUser(flatUser)
    }

    @RequestMapping(value = ["/login"], method = [RequestMethod.POST])
    fun loginUser(@RequestBody loginPass: LoginPass): String? {
        return loginService.login(loginPass.login, loginPass.pass)
    }

    @RequestMapping(value = ["/checkParcelStatus/{parcelNumber}"], method = [RequestMethod.GET])
    fun checkParcelStatus(@PathVariable("parcelNumber") number: Long): List<ParcelStatusObject> {
        return guestService.checkParcelStatus(number)
    }
}