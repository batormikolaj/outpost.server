package com.kurier.outpost.DAO

import com.kurier.outpost.domain.database.employee.Administrator
import com.kurier.outpost.domain.database.employee.Carrier
import com.kurier.outpost.domain.database.employee.Dispatcher
import com.kurier.outpost.domain.database.general.User
import com.kurier.outpost.exception.NotFoundException
import com.kurier.outpost.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class EmployeeDAO {
    @Autowired
    lateinit var employeeRepository: UserRepository

    fun saveEmployee(employee: User) {
        employeeRepository.save(employee)
    }

    fun getEmployee(id: Long): User {
        return employeeRepository.findById(id).orElseThrow { NotFoundException("No User found with id: + $id") }
    }

    fun deleteEmployee(id: Long) {
        employeeRepository.deleteById(id)
    }

    fun deleteAllEmployees(employees: MutableCollection<User>) {
        employeeRepository.deleteAll(employees)
    }

    fun findDispatchers(): MutableList<Dispatcher> {
        return employeeRepository.findDispatchers()
    }

    fun findAdministrators(): MutableList<Administrator> {
        return employeeRepository.findAdministrators()
    }

    fun findCarriers(): MutableList<Carrier> {
        return employeeRepository.findCarriers()
    }

    fun getCarrierById(id: Long): Carrier {
        return employeeRepository.findCarrierById(id)
    }

}