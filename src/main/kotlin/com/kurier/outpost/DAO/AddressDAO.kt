package com.kurier.outpost.DAO

import com.kurier.outpost.domain.database.general.Address
import com.kurier.outpost.exception.NotFoundException
import com.kurier.outpost.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Component
class AddressDAO {
    @Autowired
    lateinit var addressRepository: AddressRepository

    fun saveAddress(address: Address) {
        addressRepository.saveAndFlush(address)
    }

    fun deleteAddress(address: Address) {
        addressRepository.delete(address)
    }

    fun getAllAddresses(): List<Address> {
        return addressRepository.findAll()
    }

    fun getAddress(id: Long): Address {
        return addressRepository.findById(id).orElseThrow { NotFoundException("No Address found with id: $id") }
    }

    fun updateAddress(idAddressExisting: Long, address: Address) {
        addressRepository.updateAddress(idAddressExisting, address.city, address.house_number!!, address.postal_code, address.street, address.street_number)
    }

}