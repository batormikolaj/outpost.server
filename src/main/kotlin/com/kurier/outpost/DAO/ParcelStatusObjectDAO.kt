package com.kurier.outpost.DAO

import com.kurier.outpost.domain.database.parcel.ParcelStatusObject
import com.kurier.outpost.repository.ParcelStatusObjectRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*

@Component
class ParcelStatusObjectDAO {
    @Autowired
    lateinit var parcelStatusObjectRepository: ParcelStatusObjectRepository

    fun saveStatusObject(parcelStatusObject: ParcelStatusObject) {
        parcelStatusObjectRepository.saveAndFlush(parcelStatusObject)
    }

    fun getRecentStatus(id: Long): ParcelStatusObject {
        return parcelStatusObjectRepository.findByParcelIdOrderByTimestampDesc(id)
    }

    fun saveParcelId(parcel_id: Long, status_id: Long) {
        parcelStatusObjectRepository.updateParcelId(parcel_id, status_id)
    }

    //TODO xD Opcional<List>
    fun findById(parcelId: Long): MutableCollection<ParcelStatusObject> {
        return parcelStatusObjectRepository.findAllById(parcelId)
    }

    companion object {
        private val log = LoggerFactory.getLogger(ParcelStatusObjectDAO::class.java)
    }
}