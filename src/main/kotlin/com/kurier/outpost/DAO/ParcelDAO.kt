package com.kurier.outpost.DAO

import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.domain.database.parcel.ParcelShiftStatus
import com.kurier.outpost.domain.database.parcel.ParcelStatus
import com.kurier.outpost.domain.database.parcel.ParcelStatusObject
import com.kurier.outpost.repository.ParcelRepository
import com.kurier.outpost.repository.ParcelStatusObjectRepository
import com.kurier.outpost.repository.ShiftRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class ParcelDAO {
    @Autowired
    lateinit var parcelRepository: ParcelRepository

    @Autowired
    lateinit var parcelStatusObjectRepository: ParcelStatusObjectRepository

    @Autowired
    lateinit var shiftRepository: ShiftRepository

    fun saveParcel(parcel: Parcel) {
        parcelRepository.save(parcel)
    }

    fun deleteParcel(parcel: Parcel) {
        parcelRepository.delete(parcel)
    }

    fun getParcel(id: Long): Parcel {
        return parcelRepository.findById(id).orElseThrow { RuntimeException("No Parcel found with id: $id") }
    }

    fun getAllParcels(): List<Parcel> {
        return parcelRepository.findAll()
    }

    fun updateParcelStatus(id: Long, status: ParcelStatus) {
        val listStatus =parcelStatusObjectRepository.findAllById(id)
        val list = mutableListOf<ParcelStatusObject>()

        val oldParcel= parcelRepository.findById(id).orElseThrow { RuntimeException("No Parcel found with id: $id")}

        val statusObject = ParcelStatusObject()
        statusObject.status = status
        statusObject.timestamp = Instant.now().epochSecond
        statusObject.parcel = oldParcel

        parcelStatusObjectRepository.save(statusObject)

        listStatus.add(statusObject)

        oldParcel.status = listStatus

        parcelRepository.save(oldParcel)
    }

    fun getAllStatus(id: Long):MutableCollection<ParcelStatusObject>{
        return parcelStatusObjectRepository.findAllById(id)
    }

    fun getParcelsByStatus(status: ParcelStatus): MutableCollection<Long> {
        val x =  status.ordinal
        return parcelRepository.getParcelsByStatus(x)
    }

    fun getNotConnectedParcels(): MutableCollection<Parcel> {
        return parcelRepository.findByParcelShiftStatus(ParcelShiftStatus.NOT_CONNECTED)
    }

    fun findParcelById(idShift: Long): MutableCollection<Parcel>? {
//        return shiftParcelRepository.findByShiftId(idShift)
        return shiftRepository.findById(idShift).get().parcels
    }
}