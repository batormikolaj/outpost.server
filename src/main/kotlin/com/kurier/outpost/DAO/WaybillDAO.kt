package com.kurier.outpost.DAO

import com.kurier.outpost.domain.database.parcel.Waybill
import com.kurier.outpost.repository.WaybillRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class WaybillDAO {
    @Autowired
    lateinit var waybillRepository: WaybillRepository

    fun saveWaybill(waybill: Waybill) {
        waybillRepository.saveAndFlush(waybill)
    }

    fun getWaybill(id: Long): Waybill {
        return waybillRepository.getOne(id)
    }
}