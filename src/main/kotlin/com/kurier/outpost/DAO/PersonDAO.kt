package com.kurier.outpost.DAO

import com.kurier.outpost.domain.database.general.Person
import com.kurier.outpost.repository.PersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Component
class PersonDAO {
    @Autowired
    lateinit var personRepository: PersonRepository

    fun savePerson(person: Person) {
        personRepository.save(person)
    }

    fun deletePerson(person: Person) {
        personRepository.delete(person)
    }

    fun getPerson(id: Long): Person {
        return personRepository.findById(id).orElseThrow { RuntimeException("No person found id: $id") }
    }

    fun updatePerson(person: Person, address_id: Long) {
        personRepository.updatePerson(person.id, person.email, person.name, person.surname, person.telNumber, address_id)
    }
}