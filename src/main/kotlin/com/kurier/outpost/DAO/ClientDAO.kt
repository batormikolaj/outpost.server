package com.kurier.outpost.DAO

import com.kurier.outpost.domain.database.client.Client
import com.kurier.outpost.domain.database.parcel.Parcel
import com.kurier.outpost.repository.ClientRepository
import com.kurier.outpost.repository.ParcelRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Component
class ClientDAO {
    @Autowired
    private lateinit var clientRepository: ClientRepository

    @Autowired
    lateinit var parcelRepository: ParcelRepository

    fun addClient(client: Client) {
        clientRepository.save(client)
    }

    fun deleteClient(client: Client) {
        clientRepository.delete(client)
    }

    fun getClient(id: Long): Client {
        return clientRepository.findById(id).orElseThrow { RuntimeException("No Client found with id: $id") }
    }

    fun getMyParcels(idClient: Long): MutableCollection<Long> {
        return clientRepository.findMyParcels(idClient)
    }

}