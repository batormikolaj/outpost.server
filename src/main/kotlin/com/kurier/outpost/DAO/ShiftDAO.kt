package com.kurier.outpost.DAO

import com.kurier.outpost.domain.database.activity.Shift
import com.kurier.outpost.domain.database.activity.ShiftStatus
import com.kurier.outpost.domain.database.activity.ShiftStatusObject
import com.kurier.outpost.repository.ShiftRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.stream.Collectors

@Component
class ShiftDAO {
    @Autowired
    lateinit var shiftRepository: ShiftRepository

    fun addShift(shift: Shift) {
        shiftRepository.save(shift)
    }

    fun deleteShift(shift: Shift) {
        shiftRepository.delete(shift)
    }

    fun getShift(id: Long): Shift {
        return shiftRepository.findById(id).orElseThrow { RuntimeException("No shift found with id: $id") }
    }

    fun getMyShifts(idCarrier: Long): MutableCollection<Long> {
        return shiftRepository.findByMyId(idCarrier)
    }

    fun getShiftsWithStatus(statusInt: Int): MutableCollection<Long> {
        return shiftRepository.getShiftsWithStatus(statusInt)
    }

    fun getAllShifts(): MutableCollection<Shift> {
        return shiftRepository.findAll()
    }

    fun getCarrierFromShift(idShift: Long): Long {
        return shiftRepository.getCarrierFromShift(idShift)
    }

//    fun deleteAllCarriersFromShiftById(idShift: Long) {
//        shiftRepository.deleteCarrierFromShift(idShift)
//    }

    fun deleteAllParcelsFromShiftById(idShift: Long) {
        shiftRepository.deleteParcelsFromShift(idShift)
    }

    fun saveParcelsToShift(idShift: Long, idParcels: MutableCollection<Long>) {
        idParcels.forEach {
            shiftRepository.saveParcelsToShift(idShift, it)
        }
    }

    fun getCarrierShift(idCarrier: Long): Long {
        return shiftRepository.findParcelsFromShiftByCarrier(idCarrier)
    }
}