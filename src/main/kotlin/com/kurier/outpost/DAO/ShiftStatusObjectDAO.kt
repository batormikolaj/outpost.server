package com.kurier.outpost.DAO

import com.kurier.outpost.domain.database.activity.ShiftStatusObject
import com.kurier.outpost.repository.ShiftStatusObjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ShiftStatusObjectDAO {

    @Autowired
    lateinit var shiftStatusObjectRepository: ShiftStatusObjectRepository

    fun addShiftStatusObject(shiftStatusObject: ShiftStatusObject) {
        shiftStatusObjectRepository.save(shiftStatusObject)
    }

//    fun getAllShifts(): MutableCollection<ShiftStatusWorkObject> {
//        return shiftStatusObjectRepository.findAll()
//    }
}
