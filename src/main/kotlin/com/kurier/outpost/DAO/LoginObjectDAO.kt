package com.kurier.outpost.DAO

import com.kurier.outpost.domain.database.general.LoginObject
import com.kurier.outpost.repository.LoginRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class LoginObjectDAO {
    @Autowired
    lateinit var loginRepository: LoginRepository


    fun chechIfExists(login: String): Boolean {
        return loginRepository.existsByLogin(login)
    }

    fun saveLoginObject(loginObject: LoginObject) {
        loginRepository.saveAndFlush(loginObject)
    }

    fun editObject(loginObject: LoginObject) {
        loginRepository.saveAndFlush(loginObject)
    }


    fun changePassHash(loginObject: LoginObject) {
        loginRepository.setPassHash(loginObject.passHash, loginObject.login)
    }

    fun changeLogin(loginObject: LoginObject) {
        loginRepository.setLogin(loginObject.login, loginObject.passHash)
    }
    fun getLogin(login: String): LoginObject {
        return loginRepository.findByLogin(login).orElseThrow { (throw RuntimeException("User with provided login does not exist in data base")) }
    }
}