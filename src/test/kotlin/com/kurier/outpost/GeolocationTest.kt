package com.kurier.outpost

import com.kurier.outpost.component.AddressToGeocodeConverter
import org.junit.Test

class GeolocationTest {

    @Test
    fun getGeocode() {
        val res = AddressToGeocodeConverter().convertToGeocode("Warszawska 22, Kraków, Małopolska, Polska")
        if (res.getStatus() == "OK") {
            for (result in res.getResults()!!) {
                println("Latitude of address is: " + result.getGeometry()!!.getLocation()!!.getLat()!!)
                println("Longitude of address is: " + result.getGeometry()!!.getLocation()!!.getLng()!!)
                println("Location is " + result.getGeometry()!!.getLocation_type()!!)
            }
        } else {
            println(res.getStatus())
        }
    }

}
