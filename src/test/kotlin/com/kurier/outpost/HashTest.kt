package com.kurier.outpost

import com.kurier.outpost.component.HashGenerator
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class HashTest {
    val log = LoggerFactory.getLogger(HashTest::class.java)

    @Autowired
    lateinit var hashGenerator: HashGenerator

    @Test
    fun hashTest() {
        log.info(hashGenerator.createHash("abecadlo"))
    }
}