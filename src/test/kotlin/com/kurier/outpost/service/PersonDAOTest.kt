package com.kurier.outpost.service

import com.kurier.outpost.DAO.AddressDAO
import com.kurier.outpost.DAO.PersonDAO
import com.kurier.outpost.domain.database.general.Address
import com.kurier.outpost.domain.database.general.Person
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class PersonDAOTest {

    @Autowired
    lateinit var addressDAO: AddressDAO

    @Autowired
    lateinit var personDAO: PersonDAO

    //ready
    @Test
    fun testAddPerson() {
        val address = Address()
        address.city = "kraków"
        address.house_number = 123
        address.street_number = 456
        address.street = "Klicha"
        address.postal_code = "33-123"

        val person = Person()
        person.address = address
        person.name = "Janusz"
        person.surname = "Polak"
        person.telNumber = 666

        addressDAO.saveAddress(address)

        personDAO.savePerson(person)
        Assert.assertTrue(
                "" + personDAO.getPerson(person.id).toString() + "is saved - Id: " + personDAO.getPerson(person.id).id,
                personDAO.getPerson(person.id).id > 0
        )
    }

    //ready
    @Test(expected = RuntimeException::class)
    fun testDeletePerson() {
        val address = Address()
        address.city = "kraków"
        address.house_number = 123
        address.street_number = 456
        address.street = "Klicha"
        address.postal_code = "33-123"

        val person = Person()
        person.address = address
        person.name = "Janusz"
        person.surname = "Polak"
        person.telNumber = 666

        addressDAO.saveAddress(address)
        personDAO.savePerson(person)
        personDAO.deletePerson(person)

        //exception
        personDAO.getPerson(person.id);
    }

    //ready
    @Test
    fun testGetPerson() {
        val address = Address()
        address.city = "kraków"
        address.house_number = 123
        address.street_number = 456
        address.street = "Klicha"
        address.postal_code = "33-123"

        val person = Person()
        person.address = address
        person.name = "Janusz"
        person.surname = "Polak"
        person.telNumber = 666

        addressDAO.saveAddress(address)
        personDAO.savePerson(person)

        val msg = "No person found id: " + person.id
        val personId=person.id
        val personRepo = personDAO.getPerson(person.id)

        //assert
        Assert.assertEquals(personRepo.id, person.id)
        Assert.assertNotNull(personDAO.deletePerson(person))

        try {
            personDAO.getPerson(personId)
        } catch (e: RuntimeException) {
            Assert.assertEquals(msg, e.message)
        }
    }

}