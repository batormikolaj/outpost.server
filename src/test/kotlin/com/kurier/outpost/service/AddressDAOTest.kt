package com.kurier.outpost.service

import com.kurier.outpost.DAO.AddressDAO
import com.kurier.outpost.domain.database.general.Address
import com.kurier.outpost.exception.NotFoundException
import junit.framework.TestCase
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner


@RunWith(SpringRunner::class)
@SpringBootTest
class AddressDAOTest : TestCase() {
    val log = LoggerFactory.getLogger(AddressDAOTest::class.java)
    @Autowired
    lateinit var addressDAO: AddressDAO

    @Test(expected = NotFoundException::class)
    fun testDeleteAddress(){
        val address = Address()
        address.city = "kraków"
        address.house_number = 123
        address.street_number = 456
        address.street = "Klicha"
        address.postal_code = "33-123"

        addressDAO.saveAddress(address)
        val addressId = address.id
        addressDAO.deleteAddress(address)
        addressDAO.getAddress(addressId)
    }

    //ready
    @Test
    fun testAddAddress() {
        val address = Address()
        address.city = "kraków"
        address.house_number = 123
        address.street_number = 456
        address.street = "Klicha"
        address.postal_code = "33-123"

        addressDAO.saveAddress(address)
        val addressRepo = addressDAO.getAddress(address.id)

        //assert
        Assert.assertNotNull(addressRepo)
        Assert.assertEquals(addressRepo.id, address.id)
        Assert.assertEquals(addressRepo.city, address.city)
        Assert.assertEquals(addressRepo.street, address.street)
    }

    //ready
    @Test
    fun testGetAllAddresses() {
        val address = Address()
        address.city = "kraków1"
        address.house_number = 1
        address.street_number = 100
        address.street = "Klicha"
        address.postal_code = "33-123"

        val address1 = Address()
        address1.city = "Gdansk"
        address1.house_number = 2
        address1.street_number = 200
        address1.street = "Matecznego"
        address1.postal_code = "23-556"

        val address2 = Address()
        address2.city = "Gdansk1"
        address2.house_number = 3
        address2.street_number = 300
        address2.street = "Matecznego1"
        address2.postal_code = "33-556"

        val address3 = Address()
        address3.city = "Gdansk2"
        address3.house_number = 4
        address3.street_number = 400
        address3.street = "Matecznego2"
        address3.postal_code = "43-556"

        addressDAO.saveAddress(address)
        addressDAO.saveAddress(address1)
        addressDAO.saveAddress(address2)
        addressDAO.saveAddress(address3)

        addressDAO.getAllAddresses().map {
            log.info("${it.toString()} ")
        }
    }
}