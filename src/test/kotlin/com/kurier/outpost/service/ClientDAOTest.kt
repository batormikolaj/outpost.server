package com.kurier.outpost.service

import com.kurier.outpost.DAO.AddressDAO
import com.kurier.outpost.DAO.ClientDAO
import com.kurier.outpost.DAO.PersonDAO
import com.kurier.outpost.domain.database.client.Client
import com.kurier.outpost.domain.database.general.Address
import com.kurier.outpost.domain.database.general.Person
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class ClientDAOTest {
    @Autowired
    lateinit var personDAO: PersonDAO

    @Autowired
    lateinit var addressDAO: AddressDAO

    @Autowired
    lateinit var clientDAO: ClientDAO

    //ready
    @Test
    fun testAddClient() {
        val address = Address()
        address.city = "kraków"
        address.house_number = 123
        address.street_number = 456
        address.street = "Klicha"
        address.postal_code = "33-123"

        val person = Person()
        person.address = address
        person.name = "Janusz"
        person.surname = "Polak"
        person.telNumber = 666
        val client = Client()
        client.personalia = person


        addressDAO.saveAddress(address)
        personDAO.savePerson(person)
        clientDAO.addClient(client)

        Assert.assertNotNull(clientDAO.getClient(client.id))
    }

    //ready
    @Test
    fun testDeleteClient() {
        val address = Address()
        address.city = "kraków"
        address.house_number = 123
        address.street_number = 456
        address.street = "Klicha"
        address.postal_code = "33-123"

        val person = Person()
        person.address = address
        person.name = "Janusz"
        person.surname = "Polak"
        person.telNumber = 666
        val client = Client()
        client.personalia = person


        addressDAO.saveAddress(address)
        personDAO.savePerson(person)
        clientDAO.addClient(client)

        val newClient = clientDAO.getClient(client.id)
        val clientId = client.id
        val msg = "No Client found with id: " + client.id

        clientDAO.deleteClient(newClient)

        try {
            clientDAO.getClient(clientId)
        } catch (e: RuntimeException) {
            Assert.assertEquals(msg, e.message)
        }
    }

    //ready
    @Test
    fun testGetClient() {
        val address = Address()
        address.city = "kraków"
        address.house_number = 123
        address.street_number = 456
        address.street = "Klicha"
        address.postal_code = "33-123"

        val person = Person()
        person.address = address
        person.name = "Janusz"
        person.surname = "Polak"
        person.telNumber = 666
        val client = Client()
        client.personalia = person


        addressDAO.saveAddress(address)
        personDAO.savePerson(person)
        clientDAO.addClient(client)

        val clientId = client.id
        val newClient = clientDAO.getClient(clientId)

        assert(newClient.personalia!!.surname.equals("Polak"))
        assert(newClient.personalia!!.address!!.street.equals("Klicha"))
    }

}